<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MY_Controller {
	
  public function __construct () {
    parent::__construct();
    $this->load_recursos('SITE');
    $this->load->library('googlemaps');
    $this->inserirSM();
		
		$config['center'] = $this->data['latlon']; //Latitude - Longitude
		$config['zoom'] = '15';
		$config['apiKey'] = 'AIzaSyArC2U-5Iz7DZtL7H_NY8VlkqEdp1da19s';

		$this->googlemaps->initialize($config);
		
		$marker = array();
		$marker['position'] = $this->data['latlon'];
		$marker['infowindow_content'] = '<center><big style="font-weight: 600">SPAZIO SAPORE</big><br><br>Av. Hermann August Lepper esq.<br>com Rua Itaiópolis, nº 622<br>Bairro Saguaçu / Joinville</center>';
		$marker['icon'] = base_url().'assets/site/images/pin.png';
		$this->googlemaps->add_marker($marker);

		$this->data['map'] = $this->googlemaps->create_map();
    $this->data['redes_sociais'] = $this->settings_model->get_redes_sociais();
		$this->data['blocos']['bloco_7'] = $this->secoes_model->get(7);
		
		$this->feedInstagram();
  }
	
  public function index() {
    for ( $i = 1; $i <= 6; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }		
    $this->data['vilaGastronomica'] = $this->componente1_model->where(array('ativo' => 1))->get(1);
    $this->data['icones'] = $this->componente1_model->where(array('ativo' => 1, 'id >' => 1, 'id <' => 8))->get_all();
		
    $this->data['marcas'] = $this->marcas_model->where(array('ativo' => 1))->order_by('id', 'RANDOM')->limit(6)->get_all();
		
		$this->load_page('site', 'pages/index');
  }
	
  public function agenda_musical2() {
    for ( $i = 8; $i <= 10; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }
		
		$this->data['agenda'] = $this->agenda_model->get_agenda();
		$this->data['meses'] = array('', 'JANEIRO', 'FEVEREIRO', 'MARÇO', 'ABRIL', 'MAIO', 'JUNHO', 'JULHO', 'AGOSTO', 'SETEMBRO', 'OUTUBRO', 'NOVEMBRO', 'DEZEMBRO');
		$this->data['semana'] = array('DOMINGO', 'SEGUNDA', 'TERÇA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÁBADO');
		
    $this->load_page('site', 'pages/agenda-musical2');
  }
	
  public function agenda_musical() {
    for ( $i = 8; $i <= 10; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }
		
		$this->data['agenda'] = $this->agenda_model->get_agenda();
		$this->data['meses'] = array('', 'JANEIRO', 'FEVEREIRO', 'MARÇO', 'ABRIL', 'MAIO', 'JUNHO', 'JULHO', 'AGOSTO', 'SETEMBRO', 'OUTUBRO', 'NOVEMBRO', 'DEZEMBRO');
		$this->data['semana'] = array('DOMINGO', 'SEGUNDA', 'TERÇA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÁBADO');
		
    $this->load_page('site', 'pages/agenda-musical');
  }
	
  public function o_spazio() {
    for ( $i = 11; $i <= 13; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }
    $this->data['texto1'] = $this->componente1_model->where(array('ativo' => 1))->get(8);
    $this->data['texto2'] = $this->componente1_model->where(array('ativo' => 1))->get(9);
		
		$this->data['blocos']['bloco_6'] = $this->secoes_model->get(6);
    $this->load_page('site', 'pages/o-spazio');
  }
	
  public function contato() {
    for ( $i = 14; $i <= 16; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }
		$this->data['blocos']['bloco_6'] = $this->secoes_model->get(6);
    $this->load_page('site', 'pages/contato');
  }
	
  public function marcas() {
    for ( $i = 17; $i <= 19; $i++) {
      $this->data['blocos']['bloco_' . $i] = $this->secoes_model->get($i);
    }
		$this->data['blocos']['bloco_6'] = $this->secoes_model->get(6);
		
    $this->data['marcas'] = $this->marcas_model->where(array('ativo' => 1))->order_by('id', 'RANDOM')->get_all();
    $this->load_page('site', 'pages/marcas');
  }
	
  public function unidade( $link ) {
		$this->data['blocos']['bloco_6'] = $this->secoes_model->get(6);
		$this->data['blocos']['bloco_17'] = $this->secoes_model->get(17);
		$this->data['blocos']['bloco_20'] = $this->secoes_model->get(20);
		
		$this->data['marca'] = $this->marcas_model->where('link', $link)->get_all();
		
		if (!$this->data['marca']) {
			redirect('/marcas');
		}
    $this->load_page('site', 'pages/unidade');
  }
	
  public function enviar_newsletter () {
    if ( $this->input->post() ) {
			$config_mail = array(
    	'smtp_host' => $this->data['smtp_host'],
			'smtp_user' => $this->data['smtp_user'],
			'smtp_pass' => $this->data['smtp_pass'],
			'smtp_port' => $this->data['smtp_port'],
			'wordwrap'  => FALSE,
			'mailtype'  => 'html' );
			
			$campos = $this->input->post();
			$this->data['campos'] = $campos;
			
			ob_start();
			$this->load->view('site/forms/newsletter',$this->data);
			$html = ob_get_contents();
			ob_end_clean();
			$this->load->library('email');
			$this->email->initialize($config_mail);
			$this->email->from($this->data['smtp_user']);
			$this->email->to($this->data['smtp_user']);
			$this->email->reply_to($campos['email']);
			$this->email->subject('Cadastro da Newsletter');
			$this->email->message($html);
			
			if ( $this->email->send() ) {
				echo 'true';
			}
		} else {
			redirect('/');
		}
  }
	
  public function enviar_agenda () {
    if ( $this->input->post() ) {
			$config_mail = array(
    	'smtp_host' => $this->data['smtp_host'],
			'smtp_user' => $this->data['smtp_user'],
			'smtp_pass' => $this->data['smtp_pass'],
			'smtp_port' => $this->data['smtp_port'],
			'wordwrap'  => FALSE,
			'mailtype'  => 'html' );
			
			$campos = $this->input->post();
			$this->data['campos'] = $campos;
			
			ob_start();
			$this->load->view('site/forms/agenda',$this->data);
			$html = ob_get_contents();
			ob_end_clean();
			$this->load->library('email');
			$this->email->initialize($config_mail);
			$this->email->from($this->data['smtp_user']);
			$this->email->to($this->data['smtp_user']);
			$this->email->reply_to($campos['email'],$campos['nome']);
			$this->email->subject('Quer tocar no Spazio?');
			$this->email->message($html);
			
			if ( $this->email->send() ) {
				echo 'true';
			}
		} else {
			redirect('/');
		}
  }
	
  public function enviar_empreenda () {
    if ( $this->input->post() ) {
			$config_mail = array(
    	'smtp_host' => $this->data['smtp_host'],
			'smtp_user' => $this->data['smtp_user'],
			'smtp_pass' => $this->data['smtp_pass'],
			'smtp_port' => $this->data['smtp_port'],
			'wordwrap'  => FALSE,
			'mailtype'  => 'html' );
			
			$campos = $this->input->post();
			$this->data['campos'] = $campos;
			
			ob_start();
			$this->load->view('site/forms/empreenda',$this->data);
			$html = ob_get_contents();
			ob_end_clean();
			$this->load->library('email');
			$this->email->initialize($config_mail);
			$this->email->from($this->data['smtp_user']);
			$this->email->to($this->data['smtp_user']);
			$this->email->reply_to($campos['email'],$campos['nome']);
			$this->email->subject('Empreenda no Spazio');
			$this->email->message($html);
			
			if ( $this->email->send() ) {
				echo 'true';
			}
		} else {
			redirect('/');
		}
  }
	
  public function enviar () {
    if ( $this->input->post() ) {
			$config_mail = array(
    	'smtp_host' => $this->data['smtp_host'],
			'smtp_user' => $this->data['smtp_user'],
			'smtp_pass' => $this->data['smtp_pass'],
			'smtp_port' => $this->data['smtp_port'],
			'wordwrap'  => FALSE,
			'mailtype'  => 'html' );
			
			$campos = $this->input->post();
			$this->data['campos'] = $campos;
			
			ob_start();
			$this->load->view('site/forms/contato',$this->data);
			$html = ob_get_contents();
			ob_end_clean();
			$this->load->library('email');
			$this->email->initialize($config_mail);
			$this->email->from($this->data['smtp_user']);
			$this->email->to($this->data['smtp_user']);
			$this->email->reply_to($campos['email'], $campos['nome']);
			$this->email->subject('Contato on-line');
			$this->email->message($html);
			
			if ( $this->email->send() ) {
				echo 'true';
			}
		} else {
			redirect('/');
		}
  }
	
  public function page404 () {		
		$this->data['blocos']['bloco_1'] = $this->secoes_model->get(1);
    $this->inserirSM('Página não encontrada - 404', base_url() . 'page404', '', 'Página não encontrada, erro 404', '404');
    $this->load_page('site', 'pages/404');
  }
}