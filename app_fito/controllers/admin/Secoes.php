<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class Secoes extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->auth_admin();
		$this->load_recursos( 'ADMIN' );
		$this->set_tags( 'Seções' );
		$this->data['paginas'] = array( 'Home' => array(1, 2, 3, 4, 5, 6, 7), 
																		'O Spazio' => array(11, 12, 13),
																		'Marcas' => array(17, 18, 19),
																		'Operação' => array(20),
																		'Agenda Musical' => array(8, 9, 10),
																		'Contato' => array(14, 15, 16),
																		);
	}
	
	public function index() {
		$this->data[ 'rows' ] = $this->secoes_model->order_by('id')->get_all();
		$this->data[ 'breadcrumb' ][] = array( 'titulo' => 'Seções', 'link' => '' );
		$this->load_page( 'admin', 'secoes/index' );
	}
	
	public function ordem() {
		$this->data[ 'rows' ] = $this->secoes_model->where('id !=', 1)->order_by('ordem')->get_all();
		$this->data[ 'breadcrumb' ][] = array( 'titulo' => 'Seções', 'link' => 'secoes' );
		$this->data[ 'breadcrumb' ][] = array( 'titulo' => 'Ordem', 'link' => '' );
		$this->load_page( 'admin', 'secoes/ordem' );
	}
	
	public function ordenar() {
		if ( $this->input->post() ) {
			$ordem = $this->input->post( 'ordem' );
			foreach ( $ordem as $key => $value ) {
				$data[ 'ordem' ] = $key+2;
				$this->secoes_model->update( $data, $value );
			}
		}
		redirect( 'admin/secoes/' );
	}
	
	public function form( $id ) {
		$this->data[ 'action' ] = 'admin/secoes/form/' . $id;
		// CADASTRAR / ALTERAR
		if ( $this->input->post() ) {
			$data = $this->input->post();
			$data['classe'] = url_title( convert_accented_characters( $data[ 'tag' ] ), 'dash', TRUE );
			
			if ( isset( $_FILES[ 'foto_fundo' ] ) ) {
				$filename = character_limiter(url_title( convert_accented_characters( $data[ 'titulo' ] ), 'dash', TRUE ), 50) . '-' . time();
				$foo = new Upload( $_FILES[ 'foto_fundo' ] );
				
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert = "jpg";
					$foo->image_ratio = true;
					$foo->image_resize = true;
					$foo->image_x = 1366;
					$foo->image_y = 650;
					$dirname = './uploads/'.$this->session->userdata('cliente_site_id').'/site/secao/' . $id . '/';
					
					if ( !is_dir( $dirname ) ) {
						mkdir( $dirname, 0777, $recursive = true );
					}
					
					$foo->Process( $dirname );
					
					if ( $foo->processed ) {
						$data[ 'foto_fundo' ] = $filename . '.jpg';
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
					
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			
			$this->secoes_model->update( $data, $id );
			$this->atualiza_css();
			$this->set_alert( 'success', 'Dados salvos com sucesso!' );
		}
		
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->secoes_model->get( $id );
			if ( !$row ) {
				redirect( 'admin/home' );
			}
		} else {
			redirect( 'admin/home' );
		}
		
		$this->data[ 'row' ] = $row;
		$this->data[ 'breadcrumb' ][] = array( 'titulo' => 'Seções', 'link' => 'secoes' );
		$this->data[ 'breadcrumb' ][] = array( 'titulo' => 'Alterar', 'link' => '' );
		$this->load_form( 'secoes', 'multipart' );
	}
	
	public function remover_imagem( $id ) {
		$imagem = $this->secoes_model->get( $id );
		unlink( 'uploads/site/secao/' . $id . '/' . $imagem['foto_fundo'] );
		$data[ 'foto_fundo' ] = '';
		$this->secoes_model->update( $data, $id );
		$this->atualiza_css();
		redirect( 'admin/secoes/form/' . $id );
	}
}