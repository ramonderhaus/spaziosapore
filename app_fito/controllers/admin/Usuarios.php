<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends MY_Controller {

	public function __construct () {
		parent::__construct();
		$this->auth_admin();
		$this->load_recursos('ADMIN');
		$this->set_tags('Usuários');
	}

	public function index () {
		$this->data['rows'] = $this->users_model->get_all();
		$this->data['breadcrumb'][] = array('titulo' => 'Usuários', 'link' => '');

		$this->load_page('admin', 'users/index');
	}

	public function form ( $id = NULL )	{

		if($this->session->userdata('user_id') > 1 AND $id == 1){
			redirect('admin');
		}

		$this->data['action'] = 'admin/usuarios/form';
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');
		// CADASTRAR / ALTERAR
		if ( $this->input->post() ) {
			if ( $this->form_validation->run() == TRUE ) {
				$data = $this->input->post();
				if ( $data['senha'] != '**********' ) {
					$data['senha'] = MD5($this->input->post('senha'));
				} else {
					unset($data['senha']);
				}
				$loginDuplicado = $this->users_model->get_by_email($data['email'], $id);
				if ( !$loginDuplicado ) {
					if ( $id ) {
						$this->users_model->update( $data, $id );
					} else {
						$id = $this->users_model->insert( $data );
					}
					$this->set_alert('success', 'Dados salvos com sucesso!');
				} else {
					$this->set_alert('danger', 'Erro ao efetuar cadastro! Login já existente.');
				}
			} else {
				$this->set_alert('danger', 'Erro ao efetuar cadastro! Contate o administrador.');
			}
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$usuario = $this->users_model->get($id);
			if ( $usuario ) {
				$this->data['action']       = 'admin/usuarios/form/'.$id;
				$this->data['row']          = $usuario;
				$this->data['row']['senha'] = '**********';
			} else {
				redirect('admin/users/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Usuários', 'link' => 'usuarios');
		$this->data['breadcrumb'][] = array('titulo' => 'Cadastro', 'link' => '');
		$this->load_form('users');
	}

	public function deletar ( $id )	{
		if($this->session->userdata('user_id') > 1 AND $id == 1){
			redirect('admin');
		}

		$this->users_model->delete($id);
		redirect('admin/users/index');
	}

}