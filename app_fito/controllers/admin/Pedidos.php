<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedidos extends MY_Controller {

	public function __construct () {
		parent::__construct();
		$this->auth_admin();
		$this->load_recursos('ADMIN');
		$this->set_tags('Pedidos');
	}

	public function index () {

		$this->data['rows'] = $this->pedidos_model->get_all();

		$this->data['breadcrumb'][] = array('titulo' => 'Pedidos', 'link' => '');
		$this->load_page('admin', 'pedidos/index');
	}

	public function form ( $id = NULL )	{

		$this->data['action'] = 'admin/pedidos/form/';

		$this->form_validation->set_rules('nome', 'Nome', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');

		if ( !$id ) {
			$this->form_validation->set_rules('senha', 'Senha', 'required');
		}

    if ( $this->input->post() ) {
      if ( $this->form_validation->run() == TRUE ) {
      	$dados = $this->input->post();

      	$cliente = $this->pedidos_model->get_by_email($dados['email'], $id);

      	if ( !$cliente ) {
					if ( $id ) {
						$this->pedidos_model->update( $dados, $id );
						$this->set_alert('success', 'Cadastro alterado com sucesso!');
					} else {
						$id = $this->pedidos_model->insert( $dados );
						$this->set_alert('success', 'Cadastro efetuado com sucesso!');
					}
      	} else {
					$this->set_alert('danger', 'Erro ao efetuar cadastro! Já existe um cliente com esse E-mail cadastrado.');
				}
      } else {
      	$this->set_alert('danger', 'Erro ao efetuar cadastro! Preencha os campos corretamente.');
    	}
  	}

		// PREENCHER CAMPOS
		if ( $id ) {
			$cliente = $this->pedidos_model->get($id);
			if ( $cliente ) {
				$this->data['action']      .= $id;
				$this->data['row']          = $cliente;
				$this->data['row']['senha'] = '**********';
			} else {
				redirect('admin/pedidos/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'pedidos', 'link' => 'pedidos');
		$this->data['breadcrumb'][] = array('titulo' => 'Cadastro', 'link' => '');

		$this->load_form('pedidos', '', array('id' => 'cadastro'));
	}

	public function deletar ( $id )	{
		$this->pedidos_model->delete($id);
		redirect('admin/pedidos');
	}

}