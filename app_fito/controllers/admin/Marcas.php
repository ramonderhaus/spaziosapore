<?php
  
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  class Marcas extends MY_Controller {
    
    public function __construct () {
      parent::__construct();
      $this->auth_admin();
      $this->load_recursos('ADMIN');
      $this->set_tags('Marcas');
    }
    
    public function index () {
      $this->data['rows'] = $this->marcas_model->get_all();
      
      $this->data['breadcrumb'][] = array('titulo' => 'Marcas', 'link' => '');
      $this->load_page('admin', 'marcas/index');
    }
    
    public function form ( $id = NULL )	{
      
      $this->data['action'] = 'admin/marcas/form/';
      
      if ( $this->input->post() ) {
        
        $dados = $this->input->post();
				$dados['link'] = url_title(convert_accented_characters($dados['titulo']), 'dash', TRUE);
				$dados['ativo'] = 1;
				
				if ( isset( $_FILES[ 'logo' ] ) ) {
					$filename = url_title(convert_accented_characters($dados['titulo']), 'dash', TRUE).'-logo'.time();
					$foo = new Upload( $_FILES[ 'logo' ] );
					if ( $foo->uploaded && $foo->file_is_image ) {
						$foo->file_new_name_body = $filename;
						$foo->image_convert 	 = "png";
						$foo->image_ratio_crop   = true;
						$foo->image_resize       = true;
						$foo->image_x = 150;
						$foo->image_y = 300;

						$dirname = './uploads/site/marcas/';
						$foo->Process( $dirname );
						if ( $foo->processed ) {
							$dados[ 'logo' ] = $filename . '.png';
							$foo->clean();
						} else {
							$this->set_alert( 'danger', 'Erro no envio da logo!' );
						}
					} else {
						$this->set_alert( 'danger', 'Selecione uma logo!' );
					}
				}
				
				if ( isset( $_FILES[ 'logo2' ] ) ) {
					$filename = url_title(convert_accented_characters($dados['titulo']), 'dash', TRUE).'-logo2'.time();
					$foo = new Upload( $_FILES[ 'logo2' ] );
					if ( $foo->uploaded && $foo->file_is_image ) {
						$foo->file_new_name_body = $filename;
						$foo->image_convert 	 = "png";
						$foo->image_ratio_crop   = true;
						$foo->image_resize       = true;
						$foo->image_x = 391;
						$foo->image_ratio_y = true;

						$dirname = './uploads/site/marcas/';
						$foo->Process( $dirname );
						if ( $foo->processed ) {
							$dados[ 'logo2' ] = $filename . '.png';
							$foo->clean();
						} else {
							$this->set_alert( 'danger', 'Erro no envio da logo!' );
						}
					} else {
						$this->set_alert( 'danger', 'Selecione uma logo!' );
					}
				}
				
				if ( isset( $_FILES[ 'foto' ] ) ) {
					$filename = url_title(convert_accented_characters($dados['titulo']), 'dash', TRUE).'-foto'.time();
					$foo = new Upload( $_FILES[ 'foto' ] );
					if ( $foo->uploaded && $foo->file_is_image ) {
						$foo->file_new_name_body = $filename;
						$foo->image_convert 	 = "png";
						$foo->image_ratio_crop   = true;
						$foo->image_resize       = true;
						$foo->image_x = 1024;
						$foo->image_ratio_y = true;

						$dirname = './uploads/site/marcas/';
						$foo->Process( $dirname );
						if ( $foo->processed ) {
							$dados[ 'foto' ] = $filename . '.png';
							$foo->clean();
						} else {
							$this->set_alert( 'danger', 'Erro no envio da foto!' );
						}
					} else {
						$this->set_alert( 'danger', 'Selecione uma foto!' );
					}
				}
				
				if ( isset( $_FILES[ 'cardapio' ] ) ) {
					$filename = url_title(convert_accented_characters($dados['titulo']), 'dash', TRUE).'-cardapio'.time();
					$foo = new Upload( $_FILES[ 'cardapio' ] );
					if ( $foo->uploaded && $foo->allowed = array('application/pdf','image/*') ) {
						$foo->file_new_name_body = $filename;

						$dirname = './uploads/site/marcas/';
						$foo->Process( $dirname );
						if ( $foo->processed ) {
							$dados[ 'cardapio' ] = $filename . '.' . $foo->file_src_name_ext;
							$foo->clean();
						}
					} else {
						$this->set_alert( 'danger', 'Selecione um cardápio!' );
					}
				}
        
        if ( $id ) {
          $this->marcas_model->update( $dados, $id );
          $this->set_alert('success', 'Cadastro alterado com sucesso!');
        } else {
          $id = $this->marcas_model->insert( $dados );
          $this->set_alert('success', 'Cadastro efetuado com sucesso!');
        }
      }
      
      // PREENCHER CAMPOS
      if ( $id ) {
        $marcas = $this->marcas_model->get($id);
        if ( $marcas ) {
          $this->data['action']      .= $id;
          $this->data['row']          = $marcas;
        } else {
          redirect('admin/marcas/form/' . $id);
        }
      } else {
        $this->data['row'] = NULL;
      }
			
			$this->atualiza_css();
      
      $this->data['breadcrumb'][] = array('titulo' => 'Marcas', 'link' => 'marcas');
      $this->data['breadcrumb'][] = array('titulo' => 'Cadastro', 'link' => '');
      
      $this->load_form('marcas', 'multipart', array('id' => 'cadastro'));
    }
    
    public function deletar ( $id )	{
      $this->marcas_model->delete($id);
      redirect('admin/marcas');
    }
    
  }