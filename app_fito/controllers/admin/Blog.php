<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
    $this->set_tags('Blog');
  }

  public function index () {
    $this->data['rows'] = $this->blog_model->get_all();
	$this->data['breadcrumb'][] = array('titulo' => 'Blog', 'link' => '');
    $this->load_page('admin', 'blog/index');
  }

  public function form ( $id = NULL ) {
    $this->data['action']    = 'admin/blog/form/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
			$data = $this->input->post();
			if ( isset( $_FILES[ 'imagem' ] ) ) {
				$filename = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE).'-'.time();
				$foo = new Upload( $_FILES[ 'imagem' ] );
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert 	 = "jpg";
					$foo->image_ratio_crop   = true;
					$foo->image_resize       = true;
					$foo->image_x            = 1024;
					$foo->image_y            = 680;
					$dirname = './uploads/blog/';
					$foo->Process( $dirname );
					if ( $foo->processed ) {
						$data[ 'imagem' ] = $filename . '.jpg';
						$foo->clean();
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			$data['link'] = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE);
			if ( $id ) {
				$this->blog_model->update( $data, $id );
			} else {
				$data['ativo'] = 1;
				$id = $this->blog_model->insert( $data );
			}
			redirect('admin/blog');
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->blog_model->get($id);
			if ( $row ) {
				$this->data['action'] = 'admin/blog/form/' . $id;
				$this->data['row']    = $row;
			} else {
				redirect('admin/blog/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Blog', 'link' => 'blog');
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('blog', 'multipart');
	}

	public function remover( $id ) {
		$imagem = $this->blog_model->get( $id );
		unlink( 'uploads/blog/' . $imagem['imagem'] );

		$this->blog_model->delete( $id );
		redirect( 'admin/blog' );
	}

	public function remover_imagem( $id ) {
		$imagem = $this->blog_model->get( $id );
		unlink( 'uploads/blog/' . $imagem['imagem'] );

		$data[ 'imagem' ] = '';
		$this->blog_model->update( $data, $id );

		redirect( 'admin/blog/form/' . $id );
	}
}