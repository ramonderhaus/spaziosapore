<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Componente5 extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
    $this->set_tags('Componente Galeria');
  }

  public function index () {
    $this->data['rows'] = $this->componente5_model->get_all();
	$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
    $this->data['breadcrumb'][] = array('titulo' => 'Componente Galeria', 'link' => '');
    $this->load_page('admin', 'componente5/index');
  }

  public function form ( $id ) {
    $this->data['action']    = 'admin/componente5/form/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
			$data = $this->input->post();
			if ( isset( $_FILES[ 'imagem' ] ) ) {
				$filename = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE).'-'.time();
				$foo = new Upload( $_FILES[ 'imagem' ] );
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert 	 = "jpg";
					$foo->image_ratio_crop   = true;
					$foo->image_resize       = true;
					$foo->image_x            = 1024;
					$foo->image_y            = 680;
					$dirname = './uploads/' . $this->session->userdata('cliente_site_id') . '/componente5/';
					$foo->Process( $dirname );
					if ( $foo->processed ) {
						$data[ 'imagem' ] = $filename . '.jpg';
						$foo->clean();
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			if ( $id ) {
				$this->componente5_model->update( $data, $id );
			} else {
				$id = $this->componente5_model->insert( $data );
			}
			$this->set_alert( 'success', 'Dados salvos com sucesso!' );
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->componente5_model->get($id);
			if ( $row ) {
				$this->data['action'] = 'admin/componente5/form/' . $id;
				$this->data['row']    = $row;
			} else {
				redirect('admin/componente5/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
		$this->data['breadcrumb'][] = array('titulo' => 'Componente Galeria', 'link' => 'componente5');
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('componente5', 'multipart');
	}
	
	public function remover( $id ) {
		$imagem = $this->componente5_model->get( $id );
		unlink( 'uploads/' . $this->session->userdata('cliente_site_id') . '/componente5/' . $imagem['imagem'] );
		
		$this->componente5_model->delete( $id );
		redirect( 'admin/componente5' );
	}
	
	public function remover_imagem( $id ) {
		$imagem = $this->componente5_model->get( $id );
		unlink( 'uploads/' . $this->session->userdata('cliente_site_id') . '/componente5/' . $imagem['imagem'] );
		
		$data[ 'imagem' ] = '';
		$this->componente5_model->update( $data, $id );
		
		redirect( 'admin/componente5/form/' . $id );
	}
}