<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Componente3 extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
    $this->set_tags('Componente Equipe');
  }

  public function index () {
    $this->data['rows'] = $this->componente3_model->get_all();
	$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
    $this->data['breadcrumb'][] = array('titulo' => 'Componente Equipe', 'link' => '');
    $this->load_page('admin', 'componente3/index');
  }

  public function form ( $id ) {
    $this->data['action']    = 'admin/componente3/form/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
			$data = $this->input->post();
			if ( isset( $_FILES[ 'imagem' ] ) ) {
				$filename = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE).'-'.time();
				$foo = new Upload( $_FILES[ 'imagem' ] );
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert 	 = "jpg";
					$foo->image_ratio_crop   = true;
					$foo->image_resize       = true;
					$foo->image_x            = 1024;
					$foo->image_y            = 680;
					$dirname = './uploads/componente3/';
					$foo->Process( $dirname );
					if ( $foo->processed ) {
						$data[ 'imagem' ] = $filename . '.jpg';
						$foo->clean();
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			if ( $id ) {
				$this->componente3_model->update( $data, $id );
			} else {
				$id = $this->componente3_model->insert( $data );
			}
			$this->set_alert( 'success', 'Dados salvos com sucesso!' );
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->componente3_model->get($id);
			if ( $row ) {
				$this->data['action'] = 'admin/componente3/form/' . $id;
				$this->data['row']    = $row;
			} else {
				redirect('admin/componente3/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
		$this->data['breadcrumb'][] = array('titulo' => 'Componente Equipe', 'link' => 'componente3');
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('componente3', 'multipart');
	}

	public function remover( $id ) {
		$imagem = $this->componente3_model->get( $id );
		unlink( 'uploads/componente3/' . $imagem['imagem'] );

		$this->componente3_model->delete( $id );
		redirect( 'admin/componente3' );
	}

	public function remover_imagem( $id ) {
		$imagem = $this->componente3_model->get( $id );
		unlink( 'uploads/componente3/' . $imagem['imagem'] );

		$data[ 'imagem' ] = '';
		$this->componente3_model->update( $data, $id );

		redirect( 'admin/componente3/form/' . $id );
	}
}