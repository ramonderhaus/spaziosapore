<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Componente1 extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
  }

  public function index ($pagina) {
		$ids = $pagina == 'home' ? '<=' : '>';
    $this->set_tags('Conteúdo da página ' . ucwords(str_replace("-", " ", $pagina)));
    $this->data['rows'] = $this->componente1_model->where("id " . $ids . "", 7)->get_all();
		$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
    $this->data['breadcrumb'][] = array('titulo' => $this->data['tag_title'], 'link' => '');
    $this->load_page('admin', 'componente1/index');
  }

  public function form ( $id ) {
		$pagina = $id < 8 ? 'Home' : 'O Spazio';
    $this->set_tags('Conteúdo da página ' . ucwords(str_replace("-", " ", $pagina)));
    $this->data['action']    = 'admin/componente1/form/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
			$data = $this->input->post();
			if ( isset( $_FILES[ 'imagem' ] ) ) {
				$filename = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE).'-'.time();
				$foo = new Upload( $_FILES[ 'imagem' ] );
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert 	 = "png";
					$foo->image_ratio_crop   = true;
					$foo->image_resize       = true;
					
					if ($id == 1 || $id == 8 || $id == 9) {
						$foo->image_x = 500;
					} else {
						$foo->image_x = 200;						
					}
					$foo->image_ratio_y = true;
					
					$dirname = './assets/site/images/';
					$foo->Process( $dirname );
					if ( $foo->processed ) {
						$data[ 'imagem' ] = $filename . '.png';
						$foo->clean();
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			if ( $id ) {
				$this->componente1_model->update( $data, $id );
			} else {
				$id = $this->componente1_model->insert( $data );
			}
			$this->set_alert( 'success', 'Dados salvos com sucesso!' );
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->componente1_model->get($id);
			if ( $row ) {
				$this->data['action'] = 'admin/componente1/form/' . $id;
				$this->data['row']    = $row;
			} else {
				redirect('admin/componente1/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
		$this->data['breadcrumb'][] = array('titulo' => $this->data['tag_title'], 'link' => 'pagina/' . strtolower(str_replace(' ', '', $pagina)));
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('componente1', 'multipart');
	}
	
	public function remover_imagem( $id ) {
		$imagem = $this->componente1_model->get( $id );
		unlink( 'assets/site/images/' . $imagem['imagem'] );
		
		$data[ 'imagem' ] = '';
		$this->componente1_model->update( $data, $id );
		
		redirect( 'admin/componente1/form/' . $id );
	}
}