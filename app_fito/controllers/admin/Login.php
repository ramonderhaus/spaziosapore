<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Login extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->load_recursos('ADMIN');
    if ($this->session->userdata('admin_logado') == TRUE) {
      redirect('admin/secoes');
    }
  }

  public function index () {
    $this->load_recursos('ADMIN');
		$this->load_page('admin', 'login', array('menu' => '', 'footer' => 'footer_admin'));
	}

}