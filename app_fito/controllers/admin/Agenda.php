<?php
  
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  class Agenda extends MY_Controller {
    
    public function __construct () {
      parent::__construct();
      $this->auth_admin();
      $this->load_recursos('ADMIN');
      $this->set_tags('Agenda');
    }
    
    public function index () {
      $this->data['rows'] = $this->agenda_model->get_all();
      
      $this->data['breadcrumb'][] = array('titulo' => 'Agenda', 'link' => '');
      $this->load_page('admin', 'agenda/index');
    }
    
    public function form ( $id = NULL )	{
      
      $this->data['action'] = 'admin/agenda/form/';
      
      if ( $this->input->post() ) {

        $dados = $this->input->post();
				$data = explode("/", $dados["data"]);
				$dados['data'] = $data[2] . '-' . $data[1] . '-' . $data[0];
				$dados['ativo'] = 1;

        if ( $id ) {
          $this->agenda_model->update( $dados, $id );
          $this->set_alert('success', 'Cadastro alterado com sucesso!');
        } else {
          $id = $this->agenda_model->insert( $dados );
          $this->set_alert('success', 'Cadastro efetuado com sucesso!');
        }
      }
      
      // PREENCHER CAMPOS
      if ( $id ) {
        $agenda = $this->agenda_model->get($id);
        if ( $agenda ) {
          $this->data['action']      .= $id;
          $this->data['row']          = $agenda;
        } else {
          redirect('admin/agenda/form');
        }
      } else {
        $this->data['row'] = NULL;
      }
      
      $this->data['breadcrumb'][] = array('titulo' => 'Agenda', 'link' => 'agenda');
      $this->data['breadcrumb'][] = array('titulo' => 'Cadastro', 'link' => '');
			
			if ( $this->data['row'] ) {
				$data = explode("-", $this->data['row']["data"]);
				$this->data['data'] = $data[2] . '/' . $data[1] . '/' . $data[0];
			} else {
				$this->data['data'] = '';
			}
      
      $this->load_form('agenda', '', array('id' => 'cadastro'));
    }
    
    public function deletar ( $id )	{
      $this->agenda_model->delete($id);
      redirect('admin/agenda');
    }
    
  }