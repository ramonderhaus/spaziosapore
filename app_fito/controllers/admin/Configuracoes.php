<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
  }

  public function index () {

    $this->data['action'] = 'admin/configuracoes';

    if ( $this->input->post() ) {
      $data = $this->input->post();

      // Configurações Gerais Upload Library
      $upload_config['upload_path']    = (!is_dir('./uploads/config/')) ? mkdir('./uploads/config/', 0777, $recursive = true) : './uploads/config/';
      $upload_config['allowed_types']  = 'png';

      // Configurações Gerais Image Library
      $resize_config['image_library']  = 'gd2';
      $resize_config['maintain_ratio'] = TRUE;

      // Verifica se arquivo foi enviado no formulário
      if($_FILES['favicon']['size']){

        // Inicializa Upload Library
        $upload_config['file_name']  = 'favicon'.time();
        $this->upload->initialize($upload_config);

        // Verifica se arquivo foi enviado
        if ( $this->upload->do_upload('favicon') ) {
          $arquivo = $upload_config['file_name'] . '.png';

          // Configurações Image Library
          $resize_config['source_image']   = 'uploads/config/' . $arquivo;
          $resize_config['width']  = 256;
          $resize_config['height'] = 256;
          $this->image_lib->initialize($resize_config);

          // Verifica se imagem foi redimensionada
          if($this->image_lib->resize()){
            $data['favicon'] = $arquivo;
          } else {
            unlink($resize_config['source_image']);
            $erros[] = 'Erro no envio do Favicon: ' . $this->image_lib->display_errors();
          }
        } else {
          $erros[] = 'Erro no envio do Favicon: ' . $this->upload->display_errors();
        }
      }

      // Verifica se arquivo foi enviado no formulário
      if($_FILES['logo_principal']['size']){

        // Inicializa Upload Library
        $upload_config['file_name']  = 'logo_principal'.time();
        $this->upload->initialize($upload_config);

        // Verifica se arquivo foi enviado
        if ( $this->upload->do_upload('logo_principal') ) {
          $arquivo = $upload_config['file_name'] . '.png';

          // Configurações Image Library
          $resize_config['source_image'] = 'uploads/config/' . $arquivo;
          $resize_config['height']       = 100;
          $this->image_lib->initialize($resize_config);

          // Verifica se imagem foi redimensionada
          if($this->image_lib->resize()){
            $data['logo_principal'] = $arquivo;
          } else {
            unlink($resize_config['source_image']);
            $erros[] = 'Erro no envio do Logo Principal: ' . $this->image_lib->display_errors();
          }
        } else {
          $erros[] = 'Erro no envio do Logo Principal: ' . $this->upload->display_errors();
        }
      }

      // Verifica se arquivo foi enviado no formulário
      if($_FILES['logo_secundaria']['size']){

        // Inicializa Upload Library
        $upload_config['file_name']  = 'logo_secundaria'.time();
        $this->upload->initialize($upload_config);

        // Verifica se arquivo foi enviado
        if ( $this->upload->do_upload('logo_secundaria') ) {
          $arquivo = $upload_config['file_name'] . '.png';

          // Configurações Image Library
          $resize_config['source_image'] = 'uploads/config/' . $arquivo;
          $resize_config['height']       = 100;
          $this->image_lib->initialize($resize_config);

          // Verifica se imagem foi redimensionada
          if($this->image_lib->resize()){
            $data['logo_secundaria'] = $arquivo;
          } else {
            unlink($resize_config['source_image']);
            $erros[] = 'Erro no envio do Logo Secundária: ' . $this->image_lib->display_errors();
          }
        } else {
          $erros[] = 'Erro no envio do Logo Secundária: ' . $this->upload->display_errors();
        }
      }

      foreach ($data as $campo => $valor) {
        $this->settings_model->atualiza( $campo, $valor );
      }

      if(isset($erros)){
        $this->set_alert( 'danger', implode('<br>', $erros) );
      } else {
        $this->set_alert( 'success', 'Dados salvos com sucesso!' );
        $this->atualiza_css();
      }
    }

    $configs = $this->settings_model->get_all();

    foreach ($configs as $config) {
      $this->data['rows'][$config['campo']] = $config['valor'];
    }

    $this->data['breadcrumb'][] = array('titulo' => 'Configurações', 'link' => '');

    $this->set_tags('Configurações');
    $this->load_form('configuracoes', 'multipart');
  }

}