<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class componente6 extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
    $this->set_tags('Componente Contato');
  }

  public function index ( $id = 1 ) {
    $this->data['action']    = 'admin/componente6/index/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
		$data = $this->input->post();
		if ( $id ) {
			$this->componente6_model->update( $data, $id );
		} else {
			$id = $this->componente6_model->insert( $data );
		}
		$this->set_alert( 'success', 'Dados salvos com sucesso!' );
	}
	// PREENCHER CAMPOS
	if ( $id ) {
		$row = $this->componente6_model->get($id);
		if ( $row ) {
			$this->data['action'] = 'admin/componente6/index/' . $id;
			$this->data['row']    = $row;
		} else {
			redirect('admin/secoes/');
		}
	} else {
		$this->data['row'] = NULL;
	}

		$this->data['breadcrumb'][] = array('titulo' => 'Seções', 'link' => 'secoes');
		$this->data['breadcrumb'][] = array('titulo' => 'Componente Contato', 'link' => 'componente6');
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('componente6', 'multipart');
	}
}