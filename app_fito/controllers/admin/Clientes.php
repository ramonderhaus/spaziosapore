<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Controller {

	public function __construct () {
		parent::__construct();
		$this->auth_admin();
		$this->load_recursos('ADMIN');
		$this->set_tags('Clientes');
	}

	public function index () {
		$rows = $this->clientes_model->get_all();

		$this->data['rows'] = $rows;

		$this->data['breadcrumb'][] = array('titulo' => 'Clientes', 'link' => '');
		$this->load_page('admin', 'clientes/index');
	}

	public function form ( $id = NULL )	{

		$this->data['action'] = 'admin/clientes/form/';

		$this->form_validation->set_rules('nome', 'Nome', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');

		if ( !$id ) {
			$this->form_validation->set_rules('senha', 'Senha', 'required');
		}

    if ( $this->input->post() ) {
      if ( $this->form_validation->run() == TRUE ) {
      	$dados = $this->input->post();

      	$cliente = $this->clientes_model->get_by_email($dados['email'], $id);

      	if ( !$cliente ) {
					if ( $id ) {
						$this->clientes_model->update( $dados, $id );
						$this->set_alert('success', 'Cadastro alterado com sucesso!');
					} else {
						$id = $this->clientes_model->insert( $dados );
						$this->set_alert('success', 'Cadastro efetuado com sucesso!');
					}
      	} else {
					$this->set_alert('danger', 'Erro ao efetuar cadastro! Já existe um cliente com esse E-mail cadastrado.');
				}
      } else {
      	$this->set_alert('danger', 'Erro ao efetuar cadastro! Preencha os campos corretamente.');
    	}
  	}

		// PREENCHER CAMPOS
		if ( $id ) {
			$cliente = $this->clientes_model->get($id);
			if ( $cliente ) {
				$this->data['action']      .= $id;
				$this->data['row']          = $cliente;
				$this->data['row']['senha'] = '**********';
			} else {
				redirect('admin/clientes/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Clientes', 'link' => 'clientes');
		$this->data['breadcrumb'][] = array('titulo' => 'Cadastro', 'link' => '');

		$this->load_form('clientes', '', array('id' => 'cadastro'));
	}

	public function deletar ( $id )	{
		$this->clientes_model->delete($id);
		redirect('admin/clientes');
	}

	public function valida_cpf_cnpj () {
		if ( $this->input->post() ) {
			$data = $this->input->post();
			$cpf_cnpj = $this->clientes_model->get_cpf_cnpj($data['cpf_cnpj']);
			if ($cpf_cnpj) {
				echo $cpf_cnpj['id'];
			} else {
				echo '0';
			}
		}
	}

}