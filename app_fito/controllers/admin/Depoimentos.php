<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Depoimentos extends MY_Controller {

  public function __construct () {
    parent::__construct();
    $this->auth_admin();
    $this->load_recursos('ADMIN');
    $this->set_tags('Depoimentos');
  }

  public function index () {
    $this->data['rows'] = $this->depoimentos_model->get_all();
	$this->data['breadcrumb'][] = array('titulo' => 'Depoimentos', 'link' => '');
    $this->load_page('admin', 'depoimentos/index');
  }

  public function form ( $id = NULL ) {
    $this->data['action']    = 'admin/depoimentos/form/' . $id;
    // CADASTRAR / ALTERAR
    if ( $this->input->post() ) {
			$data = $this->input->post();
			if ( isset( $_FILES[ 'imagem' ] ) ) {
				$filename = url_title(convert_accented_characters($data['titulo']), 'dash', TRUE).'-'.time();
				$foo = new Upload( $_FILES[ 'imagem' ] );
				if ( $foo->uploaded && $foo->file_is_image ) {
					$foo->file_new_name_body = $filename;
					$foo->image_convert 	 = "jpg";
					$foo->image_ratio_crop   = true;
					$foo->image_resize       = true;
					$foo->image_x            = 400;
					$foo->image_y            = 400;
					$dirname = './uploads/' . $this->session->userdata('cliente_site_id') . '/depoimentos/';
					$foo->Process( $dirname );
					if ( $foo->processed ) {
						$data[ 'imagem' ] = $filename . '.jpg';
						$foo->clean();
					} else {
						$this->set_alert( 'danger', 'Erro no envio da foto!' );
					}
				} else {
					$this->set_alert( 'danger', 'Selecione uma foto!' );
				}
			}
			if ( $id ) {
				$this->depoimentos_model->update( $data, $id );
			} else {
				$data['ativo'] = 1;
				$id = $this->depoimentos_model->insert( $data );
			}
			redirect('admin/depoimentos');
		}
		// PREENCHER CAMPOS
		if ( $id ) {
			$row = $this->depoimentos_model->get($id);
			if ( $row ) {
				$this->data['action'] = 'admin/depoimentos/form/' . $id;
				$this->data['row']    = $row;
			} else {
				redirect('admin/depoimentos/form');
			}
		} else {
			$this->data['row'] = NULL;
		}

		$this->data['breadcrumb'][] = array('titulo' => 'Depoimentos', 'link' => 'depoimentos');
		$this->data['breadcrumb'][] = array('titulo' => 'Alterar', 'link' => '');

		$this->load_form('depoimentos', 'multipart');
	}
	
	public function remover( $id ) {
		$imagem = $this->depoimentos_model->get( $id );
		unlink( 'uploads/' . $this->session->userdata('cliente_site_id') . '/depoimentos/' . $imagem['imagem'] );
		
		$this->depoimentos_model->delete( $id );
		redirect( 'admin/depoimentos' );
	}
	
	public function remover_imagem( $id ) {
		$imagem = $this->depoimentos_model->get( $id );
		unlink( 'uploads/' . $this->session->userdata('cliente_site_id') . '/depoimentos/' . $imagem['imagem'] );
		
		$data[ 'imagem' ] = '';
		$this->depoimentos_model->update( $data, $id );
		
		redirect( 'admin/depoimentos/form/' . $id );
	}
}