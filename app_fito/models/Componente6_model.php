<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Componente6_model extends MY_Model {

	public function __construct () {
    	$this->table = 'componente6';
		parent::__construct();
	}
}
/* End of file Paginas_model.php */
/* Location: ./application/models/Paginas_model.php */