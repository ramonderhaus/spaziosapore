<?php

defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );

class Clientes_model extends MY_Model {

	public function __construct() {
    $this->table = 'clientes';
		parent::__construct();
	}

	public function get_cliente( $email, $senha ) {
		$this->db->where( 'email', $email );
		$this->db->where( 'senha', $senha );
		return $this->db->get( $this->table )->row();
	}

	public function get_by_email( $email, $id = NULL ) {
		if ( $id ) {
			$this->db->where( 'id !=', $id );
		}
		$this->db->where( 'email', $email );
		return $this->db->get( $this->table )->row();
	}

	public function get_cpf_cnpj( $numero ) {
		$query = $this->db->get_where( $this->table, array( 'cpf_cnpj' => $numero ) );
		return $query->row_array();
	}

}