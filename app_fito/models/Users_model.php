<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends MY_Model {

  public function __construct () {
    $this->table = 'users';
    parent::__construct();
  }

  public function get_by_email ( $email, $id = NULL ) {

    if ($id) {
      $this->db->where('id !=', $id);
    }

    $this->db->where('email', $email);
    return $this->db->get($this->table)->row();
  }

	public function get_cliente( $email, $senha ) {
		$this->db->where( 'email', $email );
		$this->db->where( 'senha', md5($senha) );
		return $this->db->get( $this->table )->row();
	}

}