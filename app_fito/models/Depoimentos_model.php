<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Depoimentos_model extends MY_Model {

	public function __construct () {
  	$this->table = 'depoimentos';
		parent::__construct();
	}

	public function get_depoimentos(){
		$this->db->where('ativo', 1);
		$this->db->order_by('created_at', 'DESC');
		return parent::get_all();
	}

}