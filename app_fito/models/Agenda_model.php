<?php
  
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Agenda_model extends MY_Model {
    
    public function __construct () {
      $this->table = 'agenda';
      parent::__construct();
    }

		public function get_agenda(){
			$this->select('MONTH(data) AS mes');
			$this->db->from('agenda');
			$this->db->where('ativo', 1);
			$this->db->group_by(array('YEAR(data)', 'MONTH(data)'));
			$this->db->order_by('data', 'ASC');
			$meses = $this->db->get()->result();
			foreach ($meses as $mes) {
				$this->db->select('*');
				$this->db->from('agenda');
				$this->db->where('MONTH(data)', $mes->mes);
				$this->db->order_by('data', 'ASC');
				$mes->datas = $this->db->get()->result();
			}
			return $meses;
		}
    
  }