<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class textos_model extends MY_Model {

  public function __construct () {
    $this->table = 'textos';
    parent::__construct();
  }

  public function get_by ($idioma = NULL, $secao = NULL, $campo = NULL) {
    if($secao){ $this->db->where('secao', $secao); }
    if($campo){ $this->db->where('campo', $campo); }
    if($idioma){ $this->db->where('idioma', $idioma); }
    return parent::get_all();
  }

}