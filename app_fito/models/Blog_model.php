<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends MY_Model {

	public function __construct () {
  	$this->table = 'blog';
		parent::__construct();
	}

	public function get_blog(){
		$this->db->where('ativo', 1);
		$this->db->order_by('created_at', 'DESC');
		return parent::get_all();
	}

	public function get_last(){
		return $this->db->order_by('created_at', 'DESC')->limit(1)->get($this->table)->row();
	}

}