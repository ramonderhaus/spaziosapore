<?php
  
  defined('BASEPATH') OR exit('No direct script access allowed');
  
  class Marcas_model extends MY_Model {
    
    public function __construct () {
      $this->table = 'marcas';
      parent::__construct();
    }
    
  }