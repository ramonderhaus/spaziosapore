<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Secoes_model extends MY_Model {

  public function __construct () {
    $this->table = 'secoes';
    parent::__construct();
  }

  public function get_by_tag ( $classe ) {
    $this->db->where('classe', $classe);
    return $this->db->get($this->table)->row();
  }

  public function get_secao ($classe) {
    $this->db->where('classe', $classe);
	return parent::get_all();
  }

}