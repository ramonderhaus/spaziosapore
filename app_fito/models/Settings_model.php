<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends MY_Model {

  public function __construct () {
    $this->table = 'settings';
    parent::__construct();
  }

  public function get_redes_sociais() {
    $this->db->where_in('id', array(9,10,11,12,13));
    return parent::get_all();
  }

  public function get_contato () {
    $this->db->like('campo', 'contato_');
    return parent::get_all();
  }

  public function atualiza ( $campo, $valor ) {
    $this->db->set('valor', $valor);
    $this->db->where('campo', $campo);
    $this->db->update('settings');
  }

  public function get_by_campo($campo){
    return parent::where('campo', $campo)->get();
  }

}