	<?php if ($blocos['bloco_7']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_7']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_7']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container text-center g-pb-50 g-pt-100">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="titulo g-font-size-33 g-font-size-42--md" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_7']['titulo'] ?>
					</h2>				
				</div>
				<div id="form-newsletter" class="col-12" data-animation="fadeIn" data-animation-duration="1200" data-animation-delay="800">
					<form id="f-newsletter" name="f-newsletter" method="post">
						<div class="row justify-content-center">
							<div class="col-md-8 form-group g-mb-30">
								<input name="email" class="form-control g-font-size-18 g-bg-transparent text-center rounded-0 g-brd-0 g-brd-bottom-1 g-brd-primary" type="email" placeholder="Seu melhor e-mail" autocomplete="off" required>
							</div>
							<div class="col-12">
								<button type="submit" class="btn u-btn-primary f-font-lora g-line-height-1 g-font-size-28 g-rounded-50 py-1 px-4" role="button">cadastrar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>	

	<section id="mapa" class="g-height-400">
		<?php echo $map['html']; ?>
		<div class="serrilhado"></div>
	</section>

	<footer class="text-center cor_rodape">
		<div class="container g-py-80">
			<div class="row">
				<div class="col-md-4 mb-5 mb-md-0">
					<div class="titulo"><?php echo $contato_titulo1; ?></div>
					<div class="subtitulo"><?php echo $contato_texto1; ?></div>
				</div>
				<div class="col-md-4 mb-5 mb-md-0">
					<div class="titulo"><?php echo $contato_titulo2; ?></div>
					<div class="subtitulo"><?php echo $contato_texto2; ?></div>
				</div>
				<div class="col-md-4 mb-5 mb-md-0">
					<div class="titulo"><?php echo $contato_titulo3; ?></div>
					<div class="subtitulo"><?php echo $contato_texto3; ?></div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<p>&copy; Todos os direitos reservados . Spazio Sapore 2019 <?php echo date('Y', time()) > 2019 ? ' - ' . date('Y', time()) : ''; ?> . </p>
			<p class="mb-0">criação // <a href="https://agenciainsana.com/" target="_blank">agenciainsana</a> // <a href="https://fitosistemas.com.br/" target="_blank">fitosistemas</a></p>
		</div>
	</footer>
	<a class = "js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{ "bottom": 15, "right": 15 }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn"><i class="hs-icon hs-icon-arrow-top"></i></a>
</main>
    <?php
      foreach ($assets_js as $file) {
        echo '<script src="'.base_url().'assets/site/'. $file .'.js"></script>'.PHP_EOL;
      }

			echo $map['js'];
    ?>
  </body>
</html>
