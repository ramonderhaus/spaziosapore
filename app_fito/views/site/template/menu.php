<!-- Social -->
<ul class="list-inline g-pos-abs g-top-15 g-left-10 g-z-index-2">
	<?php foreach($redes_sociais as $item) { ?>
	<?php if($item['valor']) { ?>
	<li class="list-inline-item g-mx-3">
		<a class="px-3 py-1 g-font-size-26 g-color-white" href="<?php echo $item['valor'] ?>" target="_blank">
			<i class="fa fa-<?php echo $item['campo'] ?>"></i>
		</a>
	</li>
	<?php } } ?>
</ul>
<!-- End Social -->

<!-- Responsive Toggle Button -->
<button class="btn dark-theme u-header-toggler g-bg-transparent g-pos-fix g-top-10 g-right-10 g-top-20--md g-right-20--md g-pa-0" id="header-toggler" aria-haspopup="true" aria-expanded="false" aria-controls="js-header" aria-label="Toggle Header" data-target="#js-header">
	<span class="hamburger hamburger--collapse">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
		<div>menu</div>
	</span>
</button>
<!-- End Responsive Toggle Button -->
<div id="js-header" class="u-header u-header--fullscreen--top-right" aria-labelledby="header-toggler" data-overlay-classes="g-bg-black-opacity-0_9">
	<div class="u-header__sections-container">
		<div class="u-header__section u-header__section--dark">
			<nav class="navbar">
				<div class="container">
					<div class="align-items-center flex-sm-row w-100" id="navBar">
						<ul class="js-mega-menu navbar-nav text-uppercase mx-auto g-font-size-18 g-font-size-28--md">
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>" class="nav-link">Início</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>o-spazio" class="nav-link">O Spazio</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>marcas" class="nav-link">Marcas</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>#promocoes" class="nav-link scroll">Promoções</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>agenda-musical" class="nav-link">Agenda Musical</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>contato#fale-conosco" class="nav-link scroll">Entre em contato</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>contato#empreenda" class="nav-link scroll">Empreenda no Spazio</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div
