<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $analytics ?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', '<?php echo $analytics ?>');
  </script>
  <!-- Title -->
  <title><?php echo $sm['title']; ?></title>
  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="author" content="FITO Sistemas - fitosistemas.com.br | Agência Insana - agenciainsana.com">
  <meta name="robots" content="INDEX, FOLLOW">
  <meta name="keywords" content="<?php echo $sm['keywords'] ?>"/>
  <meta name="description" content="<?php echo $sm['description'] ?>"/>
  <meta name="language" content="pt-br">
	
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>uploads/config/<?php echo $favicon ?>" type="image/x-icon" />
	
  <link rel="canonical" href="<?php echo $sm['url']; ?>" />
  <link rel="manifest" href="<?php echo base_url() ?>manifest.json">
	
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="<?php echo $this->config->item('projeto') ?>">
	<meta name="twitter:title" content="<?php echo $sm['title'] ?>">
	<meta name="twitter:description" content="<?php echo $sm['description'] ?>">
	<meta name="twitter:creator" content="FITO Sistemas">
	<!-- Twitter Summary card images must be at least 120x120px -->
	<meta name="twitter:image" content="<?php echo $sm['image'] ?>">

	<!-- Open Graph data -->
	<meta property="og:title" content="<?php echo $sm['title'] ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo $sm['url'] ?>" />
	<meta property="og:image" content="<?php echo $sm['image'] ?>" />
	<meta property="og:description" content="<?php echo $sm['description'] ?>" /> 
	<meta property="og:site_name" content="<?php echo $this->config->item('projeto') ?>" />
	
  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="<?php echo base_url() ?>uploads/config/<?php echo $favicon ?>">
  <meta name="theme-color" content="<?php echo $cor_principal; ?>">
	
  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="<?php echo $sm['title']; ?>">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>uploads/config/<?php echo $favicon ?>">
	
  <!-- Tile icon for Win8 (144x144 + tile color) -->
  <meta name="msapplication-TileImage" content="<?php echo base_url() ?>uploads/config/<?php echo $favicon ?>">
  <meta name="msapplication-TileColor" content="<?php echo $cor_principal; ?>">
	
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	
  <?php
    foreach ($assets_css as $file) {
      echo '<link href="'.base_url().'assets/site/'. $file .'.css" type="text/css" rel="stylesheet" media="all"/>' . PHP_EOL;
    }
  ?>
</head>
<body>
	<div id="splash-screen">
		<div class="center">
			<div class="loading"><div></div><div></div></div>
		</div>
	</div>
	
  <main>