<div class="row g-ma-0">
	
	<?php if ($blocos['bloco_14']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_14']['classe']; ?>" class="col-12 d-flex justify-content-center g-height-100vh dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded order-<?php echo $blocos['bloco_14']['ordem']; ?>" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="banner w-100 align-self-center">
			<div class="container text-center">
				<h1 class="subtitulo g-font-size-40 g-font-size-70--md f-font-lora" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_14']['subtitulo'] ?>
				</h1>
			</div>
		</div>
		<div class="arrow-down arrow-down-floating g-bottom-20 g-bottom-50--md" aria-hidden="true" data-animation="fadeInDown" data-animation-duration="2000" data-animation-delay="1500">
			<a class="scroll" href="#<?php echo $blocos['bloco_15']['classe'] ?>">
				<i class="arrow-down-icon"></i>
			</a>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_15']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_15']['classe'] ?>" class="col-12 px-0 order-<?php echo $blocos['bloco_15']['ordem'] ?>">
		<div class="container g-py-70">
			<div class="row justify-content-center">
				<div class="col-12 mb-5 text-center">				
					<h2 class="titulo f-font-lora g-font-size-32 g-font-size-50--md g-line-height-1" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_15']['titulo'] ?>
					</h2>
					<h3 class="subtitulo g-font-size-18 g-font-size-25--md g-line-height-1_2" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_15']['subtitulo'] ?>
					</h3>
				</div>
				<div id="contato" class="col-md-6" data-animation="fadeInUp" data-animation-duration="1800" data-animation-delay="400">
					<form id="f-contato" name="f-contato" method="post">
						<div class="row justify-content-center">
							<div class="col-12 form-group g-mb-14">
								<input name="nome" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Nome" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="email" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="email" placeholder="E-mail" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="telefone" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="tel" placeholder="Telefone com DDD" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="estado" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Estado" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="cidade" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Cidade" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="como-conheceu" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Como conheceu o Spazio?" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="assunto" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Assunto" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<textarea name="mensagem" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white g-height-280" placeholder="Mensagem" required></textarea>
							</div>
							<div class="col-12">
								<button class="btn u-btn-primary btn-block f-font-lora rounded-0 g-line-height-1 g-font-size-26 g-font-size-32--md py-2" role="button">enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_16']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_16']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_16']['ordem'] ?> dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container g-py-70">
			<div class="row justify-content-center">
				<div class="col-12 g-mb-60 text-center">				
					<h2 class="titulo mb-4 f-font-lora g-font-size-32 g-font-size-50--md g-line-height-1" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_16']['titulo'] ?>
					</h2>
					<h3 class="subtitulo g-font-size-18 g-font-size-25--md g-line-height-1_2" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_16']['subtitulo'] ?>
					</h3>
				</div>
				<div class="col-md-6 mt-2 pl-0 text-center" data-animation="fadeIn" data-animation-duration="2500" data-animation-delay="0">
					<img alt="Empreenda no Spazio Sapore" class="img-fluid" src="assets/site/images/empreenda-no-spazio.png">
				</div>
				<div id="empreenda" class="col-md-6 pt-3" data-animation="fadeInUp" data-animation-duration="1800" data-animation-delay="0">
					<form id="f-empreenda" name="f-empreenda" method="post">
						<div class="row justify-content-center">
							<div class="col-12 form-group g-mb-14">
								<input name="nome" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Nome" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="email" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="email" placeholder="E-mail" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="telefone" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="tel" placeholder="Telefone com DDD" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="estado" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Estado" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="cidade" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Cidade" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="como-conheceu" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Como conheceu o Spazio?" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="instagram" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Instagram do seu negócio" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="quanto-tempo" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Há quanto tempo possui sua empresa?" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<textarea name="carreira" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white g-height-280" placeholder="Queremos saber! Conte um pouco mais sobre seu negócio:" required></textarea>
							</div>
							<div class="col-12">
								<button type="submit" class="btn u-btn-primary btn-block f-font-lora rounded-0 g-line-height-1 g-font-size-26 g-font-size-32--md py-2" role="button">enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

</div>