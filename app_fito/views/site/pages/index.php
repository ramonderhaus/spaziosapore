<div class="row g-ma-0">
	
	<?php if ($blocos['bloco_1']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_1']['classe']; ?>" class="col-12 d-flex justify-content-center g-height-100vh dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded order-<?php echo $blocos['bloco_1']['ordem']; ?>" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="banner w-100 align-self-center">
			<div class="container text-center">
				<div class="decoration-line" data-animation="fadeInUp" data-animation-duration="1200" data-animation-delay="0"></div>
				<div class="logo" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="800">
					<img class="g-width-250 g-width-400--md" src="<?php base_url(); ?>assets/site/images/logo-spazio-sapore.png" alt="Logo Spazio Sapore">
				</div>
				<h1 class="subtitulo g-font-size-24 g-font-size-40--md" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_1']['subtitulo'] ?>
				</h1>
				<div class="decoration-line-180deg mb-4" data-animation="fadeInDown" data-animation-duration="1200" data-animation-delay="0"></div>
			</div>
		</div>
		<div class="arrow-down arrow-down-floating g-bottom-20 g-bottom-50--md" aria-hidden="true" data-animation="fadeInDown" data-animation-duration="2000" data-animation-delay="1500">
			<a class="scroll" href="#<?php echo $blocos['bloco_2']['classe'] ?>">
				<i class="arrow-down-icon"></i>
			</a>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_2']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_2']['classe'] ?>" class="col-12 g-bg-img-hero primeira-vila px-0 order-<?php echo $blocos['bloco_2']['ordem'] ?> dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container-fluid g-pt-50 g-pt-120--md">
			<div class="row">
				<div class="planta-left col p-0">
					<img class="img-fluid" src="assets/site/images/planta-180deg.png" alt="Planta Spazio Sapore" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
				</div>
				<div class="primeira-vila-container">
					<div class="container px-md-0">
						<div class="row">
							<?php if ($vilaGastronomica) { ?>
							<div class="col-md-6 pl-md-0 g-mb-80 text-center">
								<img alt="Mulher bebendo drink Spazio Sapore" class="img-fluid" src="assets/site/images/<?php echo $vilaGastronomica['imagem'] ?>" data-animation="fadeIn" data-animation-duration="2500" data-animation-delay="0">
							</div>
							<div class="col-md-6 pt-3">
								<h2 class="titulo g-font-size-42 g-font-size-45--md g-font-size-50--lg" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="200">
									<?php echo $vilaGastronomica['titulo'] ?>
								</h2>
								<div class="subtitulo g-font-size-17" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="400">
									<?php echo $vilaGastronomica['descricao'] ?>
								</div>
								<div class="saiba-mais" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="600">
									<a class="g-font-size-30" href="<?php echo base_url(); ?>o-spazio">saiba mais <span>+</span></a>
								</div>
							</div>
							<?php } ?>
							<div class="col-12">
								<div class="decoration-line g-mb-50" data-animation="fadeInUp" data-animation-duration="1200" data-animation-delay="0"></div>
							</div>
							<?php if ($icones) { ?>
							<div class="col-12">
								<div class="container">
									<div class="row icons">
										<?php foreach($icones as $icone) { ?>
										<div class="col-md-6 col-lg-4 mb-3" data-animation="fadeInUp" data-animation-duration="600" data-animation-delay="200">
											<div class="icon"><img src="<?php base_url(); ?>assets/site/images/<?php echo $icone['imagem'] ?>"></div>
											<div class="icon-titulo"><?php echo $icone['titulo'] ?></div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="col-12" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="0">
								<div class="decoration-line-180deg g-my-60 g-my-100--md"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="planta-right col p-0">
					<img class="img-fluid" src="assets/site/images/planta.png" alt="Planta Spazio Sapore" data-animation="fadeInRight" data-animation-duration="600" data-animation-delay="800">
				</div>
			</div>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_3']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_3']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_3']['ordem'] ?> marcas dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="serrilhado"></div>
		<div class="container text-center">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="titulo g-font-size-42 g-font-size-45--md g-font-size-50--lg" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="0">
						<?php echo $blocos['bloco_3']['titulo'] ?>
					</h2>
				</div>
				
				<?php $i = 1; $animation = 'Up'; foreach ($marcas as $marca) { ?>
				<div class="marca" data-animation="fadeIn<?php echo $animation; ?>" data-animation-duration="600" data-animation-delay="<?php echo $i + 1; ?>00">
					<a href="marca/<?php echo $marca['link']; ?>">
						<div class="logo <?php echo $marca['link']; ?>">
						</div>
						<div class="veja-mais">
							veja mais <span>+</span>
						</div>
					</a>	
				</div>
				<?php if ($i % 3 == 0) { $animation = 'Down'; $i = 0; ?><div class="w-100 d-none d-lg-block"></div><?php } ?>
				<?php $i++; } ?>
				
				<div class="col-12 todas-operacoes" data-animation="fadeIn" data-animation-duration="600" data-animation-delay="900">
					<a href="<?php echo base_url(); ?>marcas" class="btn btn-md-lg u-btn-outline-white">Veja todas as operações</a>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
	
<?php if ($blocos['bloco_4']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_4']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_4']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container text-center g-pb-50 g-pt-100">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="titulo g-font-size-42 g-font-size-45--md g-font-size-50--lg" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_4']['titulo'] ?>
				</h2>
				<h3 class="subtitulo g-font-size-23 g-font-size-27--md" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_4']['subtitulo'] ?>
				</h3>
			</div>
		</div>
	</div>
</section>
<?php } ?>
	
<?php if ($blocos['bloco_5']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_5']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_5']['ordem'] ?> promocoes dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<!--<div class="col-md-4 d-flex align-items-center justify-content-center p-5 f-bg-marrom">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/site/images/giant-burger.png" alt="Giant Burger - Spazio Sapore">
			</div>-->
			<div class="col-md-8 px-0">
				<article class="u-block-hover u-bg-overlay ">
					<img class="img-fluid g-height-350 g-height-auto--md u-block-hover__main--zoom-v1" src="<?php echo base_url(); ?>assets/site/images/bg-promocao.jpg" alt="Promoções - Spazio Sapore">
					<div class="u-block-hover__additional g-flex-middle g-z-index-2 g-pa-20 g-pa-40--md">
						<div class="g-flex-middle-item">							
							<h2 class="titulo g-font-size-40--lg g-font-weight-700 g-line-height-1 mb-3" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
								<?php echo $blocos['bloco_5']['titulo'] ?>
							</h2>
							<h3 class="subtitulo g-font-size-22 g-font-size-32--lg g-line-height-1_2" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
								<?php echo $blocos['bloco_5']['subtitulo'] ?>
							</h3>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>
<?php } ?>
	
<?php if ($blocos['bloco_6']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_6']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_6']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container text-center g-pb-50 g-pt-100">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="titulo g-font-size-35" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['titulo'] ?>
				</h2>
				<h3 class="subtitulo g-font-size-23 g-font-size-27--md" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['subtitulo'] ?>
				</h3>
			</div>
		</div>
	</div>
	
	<div class="js-carousel px-5 mx-1 mx-md-5 g-pb-50" data-slides-show="5" data-infinite="true" data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-40 g-height-40 u-shadow-v1-5 g-color-primary g-bg-white g-bg-primary--hover g-color-white--hover g-rounded-50x g-transition-0_2 g-transition--ease-in" data-arrow-left-classes="fa fa-angle-left fa-lg g-left-0" data-arrow-right-classes="fa fa-angle-right fa-lg g-right-0" data-responsive='[{
					 "breakpoint": 1200,
					 "settings": {
						 "slidesToShow": 5,
						 "slidesToScroll": 5
					 }
				 }, {
					 "breakpoint": 1024,
					 "settings": {
						 "slidesToShow": 4,
						 "slidesToScroll": 4
					 }
				 }, {
					 "breakpoint": 768,
					 "settings": {
						 "slidesToShow": 3,
						 "slidesToScroll": 3
					 }
				 }, {
					 "breakpoint": 576,
					 "settings": {
						 "slidesToShow": 1,
						 "slidesToScroll": 1
					 }
				 }]'>

		<?php foreach ($feedInstagram as $post) { ?>
		<div class="js-slide g-px-5">
			<article class="u-block-hover">
				<a href="<?php echo $post['link']; ?>" target="_blank">
					<figure class="u-bg-overlay">
						<img class="u-block-hover__main--zoom-v1" src="<?php echo $post['images']['low_resolution']['url']; ?>" alt="Instagram Spazio Sapore">
					</figure>
					<div class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-flex-middle">
						<div class="g-flex-middle-item">
							<div class="d-flex align-items-center justify-content-center g-font-size-12 g-mb-0 g-color-white">
								<span class="mr-3"><i class="fa fa-heart g-font-size-16 mr-1" aria-hidden="true"></i><?php echo $post['likes']['count']; ?></span>
								<span class="mr-3"><i class="fa fa-comment fa-flip-horizontal g-font-size-16 mr-1" aria-hidden="true"></i><?php echo $post['comments']['count']; ?></span>
							</div>
						</div>
					</div>
				</a>
			</article>
		</div>
		<?php } ?>
	</div>
</section>
<?php } ?>

</div>