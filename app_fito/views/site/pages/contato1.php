<div class="row g-ma-0">
	
	<?php if ($blocos['bloco_14']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_14']['classe']; ?>" class="col-12 d-flex justify-content-center g-height-100vh dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded order-<?php echo $blocos['bloco_14']['ordem']; ?>" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="banner w-100 align-self-center">
			<div class="container text-center">
				<h1 class="subtitulo f-font-lora g-font-size-80" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_14']['subtitulo'] ?>
				</h1>
			</div>
		</div>
		<div class="arrow-down arrow-down-floating g-bottom-20 g-bottom-50--md" aria-hidden="true" data-animation="fadeInDown" data-animation-duration="2000" data-animation-delay="1500">
			<a class="scroll" href="#<?php echo $blocos['bloco_15']['classe'] ?>">
				<i class="arrow-down-icon"></i>
			</a>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_15']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_15']['classe'] ?>" class="col-12 o-spazio px-0 order-<?php echo $blocos['bloco_15']['ordem'] ?> dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container-fluid g-pt-50 g-pt-120--md">
			<div class="row">
				<div class="planta-left col p-0">
					<img class="img-fluid" src="assets/site/images/planta-180deg.png" alt="Planta Spazio Sapore" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
				</div>
				<div class="primeira-vila-container">
					<div class="container px-md-0">
						<div class="row">
							<div class="col-md-6 pl-md-0 text-center" data-animation="fadeIn" data-animation-duration="2500" data-animation-delay="0">
								<img alt="Mulher sorrindo Spazio Sapore" class="img-fluid" src="assets/site/images/mulher-sorrindo-spazio.jpg">
							</div>
							<div class="col-md-6 pt-3">
								<h2 class="titulo g-font-size-42 g-font-size-55--md" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="200">
									<?php echo $blocos['bloco_15']['titulo'] ?>
								</h2>
								<div class="subtitulo g-font-size-17" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="400">
									<?php echo $blocos['bloco_15']['subtitulo'] ?>
								</div>
								<div class="saiba-mais" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="600">
									<a class="g-font-size-30" href="#!">como chegar</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="planta-right col p-0">&nbsp;</div>
			</div>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_16']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_16']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_16']['ordem'] ?> dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container-fluid g-pt-50 g-pt-120--md">
			<div class="row">
				<div class="planta-left col p-0">&nbsp;</div>
				<div class="primeira-vila-container">
					<div class="container px-md-0">
						<div class="row">
							<div class="col-md-6 order-1 order-md-0 mb-5">
								<h2 class="titulo g-font-size-42 g-font-size-55--md" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="200">
									<?php echo $blocos['bloco_16']['titulo'] ?>
								</h2>
								<div class="subtitulo g-font-size-17" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="400">
									<?php echo $blocos['bloco_16']['subtitulo'] ?>
								</div>
								<div class="saiba-mais" data-animation="fadeInDown" data-animation-duration="600" data-animation-delay="600">
									<a class="g-font-size-30" href="#!">conheça as operações</a>
								</div>
							</div>
							<div class="col-md-6 order-0 order-md-1 mb-3 text-center" data-animation="fadeIn" data-animation-duration="2500" data-animation-delay="0">
								<img alt="Prato salmão Spazio Sapore" class="img-fluid" src="assets/site/images/prato-salmao-spazio.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="planta-right col p-0">
					<img class="img-fluid" src="assets/site/images/planta.png" alt="Planta Spazio Sapore" data-animation="fadeInRight" data-animation-duration="600" data-animation-delay="800">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="decoration-line" data-animation="fadeInUp" data-animation-duration="1200" data-animation-delay="0"></div>
			</div>
		</div>
	</section>
<?php } ?>
	
<?php if ($blocos['bloco_6']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_6']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_6']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container text-center g-pb-50 g-pt-100">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="titulo g-font-size-35" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['titulo'] ?>
				</h2>
				<h3 class="subtitulo g-font-size-23 g-font-size-27--md" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['subtitulo'] ?>
				</h3>
			</div>
		</div>
	</div>
	<div class="text-center">PLUG_IN INSTAGRAM</div>
</section>
<?php } ?>

</div>