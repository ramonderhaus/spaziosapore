<div class="row g-ma-0">
	
	<?php if ($blocos['bloco_17']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_17']['classe']; ?>" class="col-12 d-flex justify-content-center g-height-100vh dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded order-<?php echo $blocos['bloco_17']['ordem']; ?>" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="banner w-100 align-self-center">
			<div class="container text-center">
				<h1 class="titulo f-font-lora g-font-size-40 g-font-size-80--md g-line-height-1_1 mb-5" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_17']['titulo'] ?>
				</h1>
				<h2 class="subtitulo g-font-size-22 g-font-size-30--md" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_17']['subtitulo'] ?>
				</h2>
			</div>
		</div>
		<div class="arrow-down arrow-down-floating g-bottom-20 g-bottom-50--md" aria-hidden="true" data-animation="fadeInDown" data-animation-duration="2000" data-animation-delay="1500">
			<a class="scroll" href="#<?php echo $blocos['bloco_18']['classe'] ?>">
				<i class="arrow-down-icon"></i>
			</a>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_18']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_18']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_18']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container text-center g-pb-50 g-pt-50 g-pt-100--md">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="titulo g-font-size-32 g-font-size-45--md g-font-size-50--lg" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_18']['titulo'] ?>
				</h2>
				<h3 class="subtitulo g-font-size-20 g-font-size-27--md" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_18']['subtitulo'] ?>
				</h3>
			</div>
		</div>
	</div>
</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_19']['ativo'] == 1) { ?>
		<section id="<?php echo $blocos['bloco_19']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_19']['ordem'] ?> marcas dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="serrilhado"></div>
		<div class="container text-center g-mb-50">
			<div class="row justify-content-center">
				<?php $i = 1; $animation = 'Up'; foreach ($marcas as $marca) { ?>
				<div class="marca" data-animation="fadeIn<?php echo $animation; ?>" data-animation-duration="600" data-animation-delay="<?php echo $i + 1; ?>00">
					<a href="marca/<?php echo $marca['link']; ?>">
						<div class="logo <?php echo $marca['link']; ?>">
						</div>
						<div class="veja-mais">
							veja mais <span>+</span>
						</div>
					</a>	
				</div>
				<?php if ($i % 3 == 0) { $animation = $animation == 'Up' ? 'Down' : 'Up'; $i = 0; ?><div class="w-100 d-none d-lg-block"></div><?php } ?>
				<?php $i++; } ?>
			</div>
		</div>
	</section>
<?php } ?>
	
<?php if ($blocos['bloco_6']['ativo'] == 1) { ?>
<section id="<?php echo $blocos['bloco_6']['classe'] ?>" class="col-12 g-bg-img-hero order-<?php echo $blocos['bloco_6']['ordem'] ?> promocao dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
	<div class="divimage dzsparallaxer--target w-100"></div>
	<div class="container text-center g-pb-50 g-pt-100">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="titulo g-font-size-35" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['titulo'] ?>
				</h2>
				<h3 class="subtitulo g-font-size-23 g-font-size-27--md" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
					<?php echo $blocos['bloco_6']['subtitulo'] ?>
				</h3>
			</div>
		</div>
	</div>
	
	<div class="js-carousel px-5 mx-1 mx-md-5 g-pb-50" data-slides-show="5" data-infinite="true" data-arrows-classes="u-arrow-v1 g-pos-abs g-top-35x g-width-40 g-height-40 u-shadow-v1-5 g-color-primary g-bg-white g-bg-primary--hover g-color-white--hover g-rounded-50x g-transition-0_2 g-transition--ease-in" data-arrow-left-classes="fa fa-angle-left fa-lg g-left-0" data-arrow-right-classes="fa fa-angle-right fa-lg g-right-0" data-responsive='[{
					 "breakpoint": 1200,
					 "settings": {
						 "slidesToShow": 5,
						 "slidesToScroll": 5
					 }
				 }, {
					 "breakpoint": 1024,
					 "settings": {
						 "slidesToShow": 4,
						 "slidesToScroll": 4
					 }
				 }, {
					 "breakpoint": 768,
					 "settings": {
						 "slidesToShow": 3,
						 "slidesToScroll": 3
					 }
				 }, {
					 "breakpoint": 576,
					 "settings": {
						 "slidesToShow": 1,
						 "slidesToScroll": 1
					 }
				 }]'>

		<?php foreach ($feedInstagram as $post) { ?>
		<div class="js-slide g-px-5">
			<article class="u-block-hover">
				<a href="<?php echo $post['link']; ?>" target="_blank">
					<figure class="u-bg-overlay">
						<img class="u-block-hover__main--zoom-v1" src="<?php echo $post['images']['low_resolution']['url']; ?>" alt="Instagram Spazio Sapore">
					</figure>
					<div class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-flex-middle">
						<div class="g-flex-middle-item">
							<div class="d-flex align-items-center justify-content-center g-font-size-12 g-mb-0 g-color-white">
								<span class="mr-3"><i class="fa fa-heart g-font-size-16 mr-1" aria-hidden="true"></i><?php echo $post['likes']['count']; ?></span>
								<span class="mr-3"><i class="fa fa-comment fa-flip-horizontal g-font-size-16 mr-1" aria-hidden="true"></i><?php echo $post['comments']['count']; ?></span>
							</div>
						</div>
					</div>
				</a>
			</article>
		</div>
		<?php } ?>
	</div>
</section>
<?php } ?>

</div>