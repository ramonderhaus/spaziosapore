<div class="row g-ma-0">
	
	<?php if ($blocos['bloco_8']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_8']['classe']; ?>" class="col-12 d-flex justify-content-center g-height-100vh dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded order-<?php echo $blocos['bloco_8']['ordem']; ?>" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="banner w-100 align-self-center">
			<div class="container text-center">
				<h1 class="subtitulo g-font-size-40 g-font-size-70--md f-font-lora" data-animation="fadeIn" data-animation-duration="800" data-animation-delay="1000">
					<?php echo $blocos['bloco_8']['subtitulo'] ?>
				</h1>
			</div>
		</div>
		<div class="arrow-down arrow-down-floating g-bottom-20 g-bottom-50--md" aria-hidden="true" data-animation="fadeInDown" data-animation-duration="2000" data-animation-delay="1500">
			<a class="scroll" href="#<?php echo $blocos['bloco_9']['classe'] ?>">
				<i class="arrow-down-icon"></i>
			</a>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_9']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_9']['classe'] ?>" class="col-12 g-z-index-1 px-0 order-<?php echo $blocos['bloco_9']['ordem'] ?>">
		<div class="container px-0 g-pt-50">
			<div class="row">
				<div class="col-12 mb-5 text-center">				
					<h2 class="titulo f-font-lora g-font-size-42 g-font-size-50--md" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_9']['titulo'] ?>
					</h2>
					<h3 class="subtitulo g-font-size-23 g-font-size-25--md g-line-height-1_2" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_9']['subtitulo'] ?>
					</h3>
				</div>
				<div class="col-md-6 order-1 order-md-0 pl-md-0 text-center" data-animation="fadeInUp" data-animation-duration="1800" data-animation-delay="400">
					<img alt="Músico Spazio Sapore" class="img-fluid" src="assets/site/images/musico.png">
				</div>
				<div class="col-md-6 order-0 order-md-1 mb-5" data-animation="fadeInUp" data-animation-duration="1800" data-animation-delay="800">
					
					<div class="js-carousel" data-autoplay="true" data-pause-hover="true" data-slides-show="1" data-infinite="true" data-arrows-classes="u-arrow-v1 g-pos-abs g-top-7 g-width-45 g-height-45 g-color-white" data-arrow-left-classes="fa fa-angle-left fa-3x g-left-20" data-arrow-right-classes="fa fa-angle-right fa-3x g-right-20">
						<?php for ($m = 1; $m <= 12; $m++) { ?>
						<div class="js-slide g-px-5">
							<div class="agenda">
								<div class="mes"><?php echo $meses[$m]; ?></div>
								<div class="data">EM DEFINIÇÃO</div>
							</div>
						</div>
						<?php } ?>
					</div>
					
				</div>
				
			</div>
		</div>
	</section>
	<?php } ?>
	
	<?php if ($blocos['bloco_10']['ativo'] == 1) { ?>
	<section id="<?php echo $blocos['bloco_10']['classe'] ?>" class="col-12 agenda g-bg-img-hero order-<?php echo $blocos['bloco_10']['ordem'] ?> dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll dzsprx-readyall loaded" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
		<div class="divimage dzsparallaxer--target w-100"></div>
		<div class="container g-py-100">
			<div class="row justify-content-center">
				<div class="col-12 g-mb-60 text-center">				
					<h2 class="titulo mb-4 f-font-lora g-font-size-42 g-font-size-50--md" data-animation="fadeInLeft" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_10']['titulo'] ?>
					</h2>
					<h3 class="subtitulo g-font-size-23 g-font-size-25--md g-line-height-1_2" data-animation="fadeInRight" data-animation-duration="1200" data-animation-delay="0">
						<?php echo $blocos['bloco_10']['subtitulo'] ?>
					</h3>
				</div>
				<div class="col-md-6 mt-2 pl-0 text-center" data-animation="fadeIn" data-animation-duration="2500" data-animation-delay="0">
					<img alt="Músico Spazio Sapore" class="img-fluid" src="assets/site/images/quer-tocar-no-spazio.png">
				</div>
				<div id="agenda" class="col-md-6 pt-3" data-animation="fadeInUp" data-animation-duration="1800" data-animation-delay="0">
					<form id="f-quer-tocar" name="f-quer-tocar" method="post">
						<div class="row justify-content-center">
							<div class="col-12 form-group g-mb-14">
								<input name="nome" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Nome" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="email" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="email" placeholder="E-mail" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="telefone" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" type="tel" placeholder="Telefone com DDD" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="estado" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Estado" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="cidade" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Cidade" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="como-conheceu" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Como conheceu o Spazio?" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="instagram" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Instagram comercial" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<input name="quanto-tempo" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white" placeholder="Há quanto tempo você toca profissionalmente?" autocomplete="off" required>
							</div>
							<div class="col-12 form-group g-mb-14">
								<textarea name="carreira" class="form-control g-font-size-14 g-font-weight-600 rounded-0 g-brd-white g-height-280" placeholder="Queremos saber! Conte um pouco mais sobre sua carreira e estilo musical:" required></textarea>
							</div>
							<div class="col-12">
								<button type="submit" class="btn u-btn-primary btn-block f-font-lora rounded-0 g-line-height-1 g-font-size-32 py-2" role="button">enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

</div>