<div style="background: #CCCCCC; color: #404040; padding: 30px">
	<div style="width: 96%; border-top: solid 6px #ba864d; margin: 0 auto; background: #fff">
		<div align="center" style="background: #1f3567; color: #fff; padding: 20px;">
			<img src="<?php echo base_url() . 'uploads/config/' . $this->config->item('logo_principal'); ?>" width="250" /><br><br>
			<h2><font face="Trebuchet MS"><u>CONTATO ON-LINE</u></font></h2>
		</div>
		<div style="padding: 15px; margin-bottom: 20px;">
			<font face="Trebuchet MS" size="2">
				<p><strong>Nome</strong>: <?php echo $campos['nome'] ?></p>
				<p><strong>E-mail</strong>: <?php echo $campos['email'] ?></p>
				<p><strong>Telefone</strong>: <?php echo $campos['telefone'] ?></p>
				<p><strong>Estado</strong>: <?php echo $campos['estado'] ?></p>
				<p><strong>Cidade</strong>: <?php echo $campos['cidade'] ?></p>
				<p><strong>Como conheceu o Spazio?</strong> <?php echo $campos['como-conheceu'] ?></p>
				<p><strong>Assunto</strong>: <?php echo $campos['assunto'] ?></p>
				<p><strong>Mensagem</strong>: <?php echo $campos['mensagem'] ?></p>
				<p>&nbsp;</p>
				<hr>
			</font>
		</div>
	</div>
</div>