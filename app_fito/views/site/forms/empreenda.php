<div style="background: #CCCCCC; color: #404040; padding: 30px">
	<div style="width: 96%; border-top: solid 6px #ba864d; margin: 0 auto; background: #fff">
		<div align="center" style="background: #1f3567; color: #fff; padding: 20px;">
			<img src="<?php echo base_url() . 'uploads/config/' . $this->config->item('logo_principal'); ?>" width="250" /><br><br>
			<h2><font face="Trebuchet MS"><u>Empreenda no Spazio</u></font></h2>
		</div>
		<div style="padding: 15px; margin-bottom: 20px;">
			<font face="Trebuchet MS" size="2">
				<p><strong>Nome</strong>: <?php echo $campos['nome'] ?></p>
				<p><strong>E-mail</strong>: <?php echo $campos['email'] ?></p>
				<p><strong>Telefone</strong>: <?php echo $campos['telefone'] ?></p>
				<p><strong>Estado</strong>: <?php echo $campos['estado'] ?></p>
				<p><strong>Cidade</strong>: <?php echo $campos['cidade'] ?></p>
				<p><strong>Como conheceu o Spazio?</strong> <?php echo $campos['como-conheceu'] ?></p>
				<p><strong>Instagram do seu negócio</strong>: <?php echo $campos['instagram'] ?></p>
				<p><strong>Há quanto tempo possui sua empresa?</strong>: <?php echo $campos['quanto-tempo'] ?></p>
				<p><strong>Queremos saber! Conte um pouco mais sobre seu negócio:</strong>: <?php echo $campos['carreira'] ?></p>
				<p>&nbsp;</p>
				<hr>
			</font>
		</div>
	</div>
</div>