<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="cpf">CPF/CNPJ <span id="status" class="text-danger"></span></label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="cpf_cnpj" id="cpf" value="<?php echo $row['cpf_cnpj'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" <?php echo ($row['cpf_cnpj']) ? 'disabled' : ''; ?> required>
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="nome">Nome</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="nome" id="nome" value="<?php echo $row['nome'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="email">E-mail</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="email" id="email" value="<?php echo $row['email'] ?>" type="email" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-6 form-group g-mb-30">
          <label class="g-mb-10" for="senha">Senha</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="senha" id="senha" placeholder="************" type="password" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-6 form-group g-mb-30">
          <label class="g-mb-10" for="confirmar_senha">Confirmar Senha</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input id="confirmar_senha" placeholder="************" type="password" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="whatsapp">WhatsApp</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="whatsapp" id="whatsapp" value="<?php echo $row['whatsapp'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10 cel" required>
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="celular">Celular</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="celular" id="celular" value="<?php echo $row['celular'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10 cel">
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="telefone">Telefone</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="telefone" id="telefone" value="<?php echo $row['telefone'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10 tel">
          </div>
        </div>

        <div class="col-md-3 form-group g-mb-30">
          <label class="g-mb-10" for="cep">CEP</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="cep" id="cep" value="<?php echo $row['cep'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10 cep">
          </div>
        </div>

        <div class="col-md-6 form-group g-mb-30">
          <label class="g-mb-10" for="endereco">Endereço</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="endereco" id="endereco" value="<?php echo $row['endereco'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-3 form-group g-mb-30">
          <label class="g-mb-10" for="complemento">Complemento</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="complemento" id="complemento" value="<?php echo $row['complemento'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="bairro">Bairro</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="bairro" id="bairro" value="<?php echo $row['bairro'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="cidade">Cidade</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="cidade" id="cidade" value="<?php echo $row['cidade'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10">Estado</label>
          <div class="form-group u-select--v3 g-bg-white g-pos-rel g-brd-gray-light-v3 g-rounded-4 mb-0">
            <select title="Estado" name="uf" class="js-select u-select--v3-select u-sibling w-100" style="display: none;">
              <option value="AC" <?php echo ($row['uf'] == 'AC') ? ' selected' : ''; ?> data-content='Acre'>Acre</option>
              <option value="AL" <?php echo ($row['uf'] == 'AL') ? ' selected' : ''; ?> data-content='Alagoas'>Alagoas</option>
              <option value="AP" <?php echo ($row['uf'] == 'AP') ? ' selected' : ''; ?> data-content='Amapá'>Amapá</option>
              <option value="AM" <?php echo ($row['uf'] == 'AM') ? ' selected' : ''; ?> data-content='Amazonas'>Amazonas</option>
              <option value="BA" <?php echo ($row['uf'] == 'BA') ? ' selected' : ''; ?> data-content='Bahia'>Bahia</option>
              <option value="CE" <?php echo ($row['uf'] == 'CE') ? ' selected' : ''; ?> data-content='Ceará'>Ceará</option>
              <option value="DF" <?php echo ($row['uf'] == 'DF') ? ' selected' : ''; ?> data-content='Distrito Federal'>Distrito Federal</option>
              <option value="ES" <?php echo ($row['uf'] == 'ES') ? ' selected' : ''; ?> data-content='Espírito Santo'>Espírito Santo</option>
              <option value="GO" <?php echo ($row['uf'] == 'GO') ? ' selected' : ''; ?> data-content='Goiás'>Goiás</option>
              <option value="MA" <?php echo ($row['uf'] == 'MA') ? ' selected' : ''; ?> data-content='Maranhão'>Maranhão</option>
              <option value="MT" <?php echo ($row['uf'] == 'MT') ? ' selected' : ''; ?> data-content='Mato Grosso'>Mato Grosso</option>
              <option value="MS" <?php echo ($row['uf'] == 'MS') ? ' selected' : ''; ?> data-content='Mato Grosso do Sul'>Mato Grosso do Sul</option>
              <option value="MG" <?php echo ($row['uf'] == 'MG') ? ' selected' : ''; ?> data-content='Minas Gerais'>Minas Gerais</option>
              <option value="PA" <?php echo ($row['uf'] == 'PA') ? ' selected' : ''; ?> data-content='Pará'>Pará</option>
              <option value="PB" <?php echo ($row['uf'] == 'PB') ? ' selected' : ''; ?> data-content='Paraíba'>Paraíba</option>
              <option value="PR" <?php echo ($row['uf'] == 'PR') ? ' selected' : ''; ?> data-content='Paraná'>Paraná</option>
              <option value="PE" <?php echo ($row['uf'] == 'PE') ? ' selected' : ''; ?> data-content='Pernambuco'>Pernambuco</option>
              <option value="PI" <?php echo ($row['uf'] == 'PI') ? ' selected' : ''; ?> data-content='Piauí'>Piauí</option>
              <option value="RJ" <?php echo ($row['uf'] == 'RJ') ? ' selected' : ''; ?> data-content='Rio de Janeiro'>Rio de Janeiro</option>
              <option value="RN" <?php echo ($row['uf'] == 'RN') ? ' selected' : ''; ?> data-content='Rio Grande do Norte'>Rio Grande do Norte</option>
              <option value="RS" <?php echo ($row['uf'] == 'RS') ? ' selected' : ''; ?> data-content='Rio Grande do Sul'>Rio Grande do Sul</option>
              <option value="RO" <?php echo ($row['uf'] == 'RO') ? ' selected' : ''; ?> data-content='Rondônia'>Rondônia</option>
              <option value="RR" <?php echo ($row['uf'] == 'RR') ? ' selected' : ''; ?> data-content='Roraima'>Roraima</option>
              <option value="SC" <?php echo ($row['uf'] == 'SC') ? ' selected' : ''; ?> data-content='Santa Catarina'>Santa Catarina</option>
              <option value="SP" <?php echo ($row['uf'] == 'SP') ? ' selected' : ''; ?> data-content='São Paulo'>São Paulo</option>
              <option value="SE" <?php echo ($row['uf'] == 'SE') ? ' selected' : ''; ?> data-content='Sergipe'>Sergipe</option>
              <option value="TO" <?php echo ($row['uf'] == 'TO') ? ' selected' : ''; ?> data-content='Tocantins'>Tocantins</option>
            </select>
            <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
              <i class="hs-admin-angle-down"></i>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>