<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">
        <div class="form-group col-md-3 g-mb-30">
          <label class="g-mb-10">Ícone</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Ícone" value="<?php echo $row["icone"]; ?>" name="icone" id="icone">
        </div>
        <div class="form-group col-md-9">
          <label class="g-mb-10">Título</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título" value="<?php echo $row["titulo"]; ?>" name="titulo" id="titulo">
        </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Descrição</label>
          <textarea class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" rows="7" placeholder="Descrição" name="descricao" id="descricao"><?php echo $row["descricao"]; ?></textarea>
        </div>
        <div class="form-group col-md-3 g-mb-30">
          <label class="g-mb-10">Ícone 2</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Ícone 2" value="<?php echo $row["icone2"]; ?>" name="icone2" id="icone2">
        </div>
        <div class="form-group col-md-9">
          <label class="g-mb-10">Título 2</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título 2" value="<?php echo $row["titulo2"]; ?>" name="titulo2" id="titulo2">
        </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Descrição 2</label>
          <textarea class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" rows="7" placeholder="Descrição 2" name="descricao2" id="descricao2"><?php echo $row["descricao2"]; ?></textarea>
        </div>
        <div class="form-group col-md-3 g-mb-30">
          <label class="g-mb-10">Ícone 3</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Ícone 3" value="<?php echo $row["icone3"]; ?>" name="icone3" id="icone3">
        </div>
        <div class="form-group col-md-9">
          <label class="g-mb-10">Título 3</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título 2" value="<?php echo $row["titulo3"]; ?>" name="titulo2" id="titulo2">
        </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Descrição 3</label>
          <textarea class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" rows="7" placeholder="Descrição 3" name="descricao3" id="descricao3"><?php echo $row["descricao3"]; ?></textarea>
        </div>
      </div>
    </div>
  </div>
</div>