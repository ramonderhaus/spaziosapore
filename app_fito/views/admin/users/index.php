<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
  <h1 class="g-font-weight-300 g-color-primary g-mb-28"><?php echo $tag_title ?></h1>
  <div class="row">
    <div class="col-md-12">
      <div class="media-md align-items-center g-mb-30">
        <div class="d-flex g-mb-15 g-mb-0--md">
          <h3 class="g-font-weight-400 g-font-size-16 g-color-black mb-0"><a href="<?php echo base_url() ?>admin/usuarios/form" class="btn u-btn-primary g-mr-10 g-mb-15">Cadastrar <i class="hs-admin-plus"></i></a></h3>
        </div>
      </div>
      <div class="table-responsive g-mb-40">
        <table class="js-datatable table table-hover table-bordered g-color-black">
          <thead>
            <tr>
              <th>#</th>
              <th>Tipo</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($rows) { ?>
              <?php foreach ($rows as $row) { ?>
                <tr>
                  <td class="text-right text-md-center"><?php echo $row['id'] ?></td>
                <td class="g-px-30">
                    <div class="d-inline-block">
                      <span class="d-flex align-items-center justify-content-center u-tags-v1 g-brd-around g-bg-gray-light-v8 g-bg-gray-light-v8 g-font-size-default g-color-gray-dark-v6 g-rounded-50 g-py-4 g-px-15">
                      <span class="u-badge-v2--md g-pos-stc g-transform-origin--top-left g-bg-lightblue-v3 g-mr-8"></span>
                      ADMIN
                      </span>
                    </div>
                  </td>
                  <td><?php echo $row['nome'] ?></td>
                  <td><?php echo $row['email'] ?></td>
                  <td class="d-flex align-items-center justify-content-center">
										<div class="g-mr-10">
											<a href="<?php echo base_url() ?>admin/usuarios/form/<?php echo $row['id'] ?>" class="btn u-btn-outline-blue rounded-circle" data-toggle="tooltip" data-placement="top" title="Editar">
												<i class="fa fa-edit" aria-hidden="true"></i>
											</a>
										</div>
										<div>
											<a href="<?php echo base_url() ?>admin/usuarios/deletar/<?php echo $row['id'] ?>" class="btn btn-outline-danger rounded-circle remove" data-toggle="tooltip" data-placement="top" title="Excluir">
												<i class="fa fa-trash" aria-hidden="true"></i>
											</a>
										</div>
                  </td>
                </tr>
              <?php } ?>
            <?php } else { ?>
              <tr><td colspan="6">Nenhum registro encontrado.</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>

    </div>

  </div>
</div>