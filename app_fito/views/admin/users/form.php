<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">

        <div class="col-md-12 form-group g-mb-30">
          <label class="g-mb-10" for="nome">Nome</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="nome" id="nome" value="<?php echo $row['nome'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-12 form-group g-mb-30">
          <label class="g-mb-10" for="email">E-mail</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="email" id="email" value="<?php echo $row['email'] ?>" type="email" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-6 form-group g-mb-30">
          <label class="g-mb-10" for="senha">Senha</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="senha" id="senha" placeholder="************" type="password" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

        <div class="col-md-6 form-group g-mb-30">
          <label class="g-mb-10" for="senha">Confirmar Senha</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="senha" id="senha" placeholder="************" type="password" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10">
          </div>
        </div>

      </div>
    </div>
  </div>
</div>