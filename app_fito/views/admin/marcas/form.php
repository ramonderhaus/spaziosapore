<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">
        <div class="form-group col-md-3">
          <label class="g-mb-10">Logo ( <?php echo ($row["logo"] == "") ? "<em style='color: #ccc'>sem logo</em>" : "<a href='javascript:(void);' data-toggle='modal' data-target='#myLogo'>Ver logo</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="logo" id="logo" type="file" accept="image/*">
            </div>
          </div>
          <div class="modal fade" id="myLogo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Ver logo</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body text-center" style="background: #ccc">
                  <img src="<?php echo base_url() . 'uploads/site/marcas/' . $row["logo"]; ?>" class="img-fluid">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <a href="<?php echo base_url() ?>admin/marcas/remover_logo/<?php echo $row['id'] ?>" class="btn btn-primary remove">Apagar</a>
                </div>
              </div>
            </div>
          </div>
				</div>
        <div class="form-group col-md-3">
          <label class="g-mb-10">Logo 2 ( <?php echo ($row["logo2"] == "") ? "<em style='color: #ccc'>sem logo 2</em>" : "<a href='javascript:(void);' data-toggle='modal' data-target='#myLogo2'>Ver logo 2</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="logo2" id="logo2" type="file" accept="image/*">
            </div>
          </div>
          <div class="modal fade" id="myLogo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Ver logo 2</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body text-center" style="background: #ccc">
                  <img src="<?php echo base_url() . 'uploads/site/marcas/' . $row["logo2"]; ?>" class="img-fluid">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <a href="<?php echo base_url() ?>admin/marcas/remover_logo2/<?php echo $row['id'] ?>" class="btn btn-primary remove">Apagar</a>
                </div>
              </div>
            </div>
          </div>
				</div>
        <div class="form-group col-md-3">
          <label class="g-mb-10">Foto ( <?php echo ($row["foto"] == "") ? "<em style='color: #ccc'>sem foto</em>" : "<a href='javascript:(void);' data-toggle='modal' data-target='#myFoto'>Ver foto</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="foto" id="foto" type="file" accept="image/*">
            </div>
          </div>
          <div class="modal fade" id="myFoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Ver foto</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  <img src="<?php echo base_url() . 'uploads/site/marcas/' . $row["foto"]; ?>" class="img-fluid">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <a href="<?php echo base_url() ?>admin/marcas/remover_foto/<?php echo $row['id'] ?>" class="btn btn-primary remove">Apagar</a>
                </div>
              </div>
            </div>
          </div>
				</div>
        <div class="form-group col-md-3">
          <label class="g-mb-10">Cardápio ( <?php echo ($row["cardapio"] == "") ? "<em style='color: #ccc'>sem cardápio</em>" : "<a href='" . base_url() . "uploads/site/marcas/" . $row["cardapio"] . "' target='_blank'>Ver cardápio</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="cardapio" id="cardapio" type="file" accept="image/*,application/pdf">
            </div>
					</div>
				</div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Título</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título" value="<?php echo $row["titulo"]; ?>" name="titulo" id="titulo">
        </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Descrição</label>
          <textarea class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" rows="7" placeholder="Descrição" name="descricao" id="descricao"><?php echo $row["descricao"]; ?></textarea>
        </div>
      </div>
    </div>
  </div>
</div>