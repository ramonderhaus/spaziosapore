
<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
  <h1 class="g-font-weight-300 g-color-primary g-mb-28"><?php echo $tag_title ?></h1>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive g-mb-40">
        <table class="js-datatable table table-hover table-bordered g-color-black">
          <thead>
            <tr>
              <th class="g-width-120">#</th>
              <th>Título</th>
              <th class="g-width-120">Status</th>
              <th class="g-width-120">Ações</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($rows) { ?>
              <?php foreach ($rows as $row) { ?>
                <tr>
                  <td class="text-right text-md-center"><?php echo $row['id'] ?></td>
                  <td><?php echo $row['titulo'] ?></td>
                  <td align="center">
					<label class="form-check-inline u-check g-mr-20 mx-0 mb-0">
						<input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" id="ativo<?php echo $row['id'] ?>" <?php echo ($row['ativo'] == 1) ? 'checked' : '' ?> type="checkbox">
						<div class="u-check-icon-radio-v7">
							<i class="fa ativo" data-check-icon="&#xf00c" data-uncheck-icon="&#xf00d" data-id="<?php echo $row['id'] ?>" data-table="componente1"></i>
						</div>
					</label>
				  </td>
                  <td align="center">
                  <div class="g-pos-rel g-top-3 d-inline-block">
                    <a id="submenu<?php echo $row['id'] ?>" class="u-link-v5 g-line-height-0 g-font-size-24 g-color-gray-light-v6 g-color-lightblue-v3--hover" href="#!" aria-controls="dropDown<?php echo $row['id'] ?>" aria-haspopup="true" aria-expanded="false" data-dropdown-event="click" data-dropdown-target="#dropDown<?php echo $row['id'] ?>">
                      <i class="hs-admin-align-justify"></i>
                    </a>

                    <div id="dropDown<?php echo $row['id'] ?>" class="u-shadow-v31 g-pos-abs g-right-0 g-z-index-2 g-bg-white" aria-labelledby="submenu<?php echo $row['id'] ?>">
                      <ul class="list-unstyled g-nowrap mb-0">
                        <li>
                          <a href="<?php echo base_url() ?>admin/componente1/form/<?php echo $row['id'] ?>" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14">
                            <i class="hs-admin-pencil g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Editar
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>