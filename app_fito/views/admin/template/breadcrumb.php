<div class="g-hidden-sm-down g-bg-gray-light-v8 g-pa-20">
  <ul class="u-list-inline g-color-gray-dark-v6">

    <?php array_unshift($breadcrumb, array('titulo' => 'Dashboard', 'link' => 'dashboard')); ?>

    <?php foreach ($breadcrumb as $item) { ?>

      <li class="list-inline-item g-mr-10">

        <?php if ( $item['link'] != '' ) { ?>
          <a class="u-link-v5 g-color-gray-dark-v6 g-color-lightblue-v3--hover g-valign-middle" href="<?php echo base_url(); ?>admin/<?php echo $item['link'] ?>"><?php echo $item['titulo'] ?></a>
        <?php } else { ?>
          <span class="g-valign-middle"><?php echo $item['titulo'] ?></span>
        <?php } ?>

        <?php if(end($breadcrumb) != $item){ ?>
          <i class="hs-admin-angle-right g-font-size-12 g-color-gray-light-v6 g-valign-middle g-ml-10"></i>
        <?php } ?>

      </li>

    <?php } ?>

  </ul>
</div>
