
<header id="js-header" class="u-header u-header--sticky-top">
  <div class="u-header__section u-header__section--admin-dark g-min-height-65">

    <nav class="navbar no-gutters g-pa-0">
      <div class="col-auto d-flex flex-nowrap u-header-logo-toggler g-py-12">
        <a href="<?php echo base_url(); ?>admin" class="navbar-brand d-flex mx-auto g-hidden-xs-down g-line-height-1 py-0 g-mt-5">
          <img src="<?php echo base_url() . 'uploads/config/' . $this->config->item('logo_principal'); ?>" class="h-100">
        </a>
        <a class="js-side-nav u-header__nav-toggler d-flex align-self-center ml-auto" href="#!" data-hssm-class="u-side-nav--mini u-sidebar-navigation-v1--mini" data-hssm-body-class="u-side-nav-mini" data-hssm-is-close-all-except-this="true" data-hssm-target="#sideNav">
          <i class="hs-admin-align-left"></i>
        </a>
      </div>

      <!-- Messages/Notifications/Top Search Bar/Top User -->
      
<!-- End Messages/Notifications/Top Search Bar/Top User -->

</nav>
</div>
</header>
<section class="container-fluid px-0 g-pt-65">

  <div class="row no-gutters g-pos-rel g-overflow-hidden">

    <div id="sideNav" class="col-auto u-sidebar-navigation-v1 u-sidebar-navigation--dark">
      <ul id="sideNavMenu" class="u-sidebar-navigation-v1-menu u-side-nav--top-level-menu g-min-height-100vh mb-0">

        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/secoes">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-layers"></i>
            </span>
            <span class="media-body align-self-center">Seções</span>
          </a>
        </li>
        
        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/marcas">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-id-badge"></i>
            </span>
            <span class="media-body align-self-center">Marcas</span>
          </a>
        </li>

        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/agenda">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-id-badge"></i>
            </span>
            <span class="media-body align-self-center">Agenda Musical</span>
          </a>
        </li>

        <!--<li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/clientes">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-id-badge"></i>
            </span>
            <span class="media-body align-self-center">Clientes</span>
          </a>
        </li>-->

        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/configuracoes">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-settings"></i>
            </span>
            <span class="media-body align-self-center">Configurações</span>
          </a>
        </li>

        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" href="<?php echo base_url() ?>admin/usuarios">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-user"></i>
            </span>
            <span class="media-body align-self-center">Usuários</span>
          </a>
        </li>

        <li class="u-sidebar-navigation-v1-menu-item u-side-nav--top-level-menu-item">
          <a class="media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12"
             href="<?php echo base_url(); ?>admin/dashboard/logoff_admin">
            <span class="d-flex align-self-center g-font-size-18 g-mr-18">
              <i class="hs-admin-shift-right"></i>
            </span>
            <span class="media-body align-self-center">SAIR</span>
          </a>
        </li>

      </ul>
    </div>

    <div class="col g-ml-45 g-ml-0--lg g-pb-65--md">
