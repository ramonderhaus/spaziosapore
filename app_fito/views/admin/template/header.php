<!DOCTYPE html>
<html lang="pt-BR">

<head>

  <title><?php echo $tag_title ?></title>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>uploads/config/<?php echo $favicon ?>" type="image/x-icon" />
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <?php
      foreach ($assets_css as $file){
        echo '<link href="'.base_url().'assets/admin/'. $file .'.css" type="text/css" rel="stylesheet"  media="screen,projection,print"/>';
      }
    ?>

</head>
<body>
  <main>
