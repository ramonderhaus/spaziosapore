
<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
  <h1 class="g-font-weight-300 g-color-primary g-mb-0"><?php echo $tag_title ?></h1>

  <div class="row">

    <div class="col-md-12">
      <?php if (isset($resposta)) { ?>
          <div class="alert alert-<?php echo $classe ?> alert-dismissable col-md-4 g-mt-20 mx-auto"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong><?php echo $resposta ?></strong></div>
      <?php } ?>

      <?php echo ($form == 'multipart') ? form_open_multipart($action, $parametro) : form_open($action, $parametro) ?>

        <?php $this->load->view('admin/' . $pagina); ?>

        <div class="pull-right">
          <a href="#!" onClick="history.go(-1)" class="btn btn-md btn-xl--md u-btn-outline-gray-dark-v6 g-font-size-12 g-font-size-default--md g-mr-10 g-mb-10">Cancelar</a>
          <button class="btn btn-md btn-xl--md u-btn-lightblue-v3 g-width-160--md g-font-size-12 g-font-size-default--md g-mb-10" type="submit">Salvar</button>
        </div>

      <?php echo form_close() ?>
    </div>

  </div>
</div>