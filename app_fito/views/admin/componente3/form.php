<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">
        <div class="form-group col-sm-12">
          <label class="g-mb-10">Imagem ( <?php echo ($row["imagem"] == "") ? "<em style='color: #ccc'>sem foto</em>" : "<a href='javascript:(void);' data-toggle='modal' data-target='#myModal2'>Ver foto</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="imagem" id="imagem" type="file">
            </div>
          </div>
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Ver foto</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  <img src="<?php echo base_url() . 'uploads/componente3/' . $row["imagem"]; ?>" class="img-fluid">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <a href="<?php echo base_url() ?>admin/componente3/remover_imagem/<?php echo $row['id'] ?>" class="btn btn-primary remove">Apagar</a>
                </div>
              </div>
            </div>
          </div>
	  </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Função</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título" value="<?php echo $row["titulo"]; ?>" name="titulo" id="titulo">
        </div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Nome</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título" value="<?php echo $row["descricao"]; ?>" name="descricao" id="descricao">
        </div>
      </div>
    </div>
  </div>
</div>