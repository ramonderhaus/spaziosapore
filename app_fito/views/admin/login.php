<div class="login w-100 h-100">
  <div class="g-min-height-100vh">
    <div class="login-panel panel panel-default u-shadow-v1-5 g-rounded-3 g-bg-black-opacity-0_7">
      <div class="panel-heading" id="logo">
        <img class="img-fluid g-px-50 g-py-30" src="<?php echo base_url() . 'uploads/config/' . $this->config->item('logo_principal'); ?>" alt="<?php echo $tag_title ?>" />
      </div>
      <div class="panel-body">

        <?php if($this->session->userdata('alert_msg')){ ?>
          <div class="alert alert-<?php echo $this->session->userdata('alert_class') ?> alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong><?php echo $this->session->userdata('alert_msg') ?></strong></div>
        <?php } ?>

        <form role="form" method="post" action="<?php echo base_url() ?>admin/login/auth_admin">
          <fieldset>
            <div class="form-group">
              <input class="form-control g-pa-10-15" placeholder="E-mail" name="email" type="email" autofocus required="required">
            </div>
            <div class="form-group">
              <input class="form-control g-pa-10-15" placeholder="Senha" name="senha" type="password" required="required">
            </div>
            <button type="submit" class="btn btn-success btn-block g-pa-10-15 g-mb-30">Login</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>