<div class="col-md-12">
  <div class="g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

    <h4 class="d-flex align-self-center text-uppercase g-font-weight-300 g-color-black g-mb-20">Geral</h4>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Título do Site</label>
      <div class="g-pos-rel">
        <input type="text" value="<?php echo $rows['tag_title'] ?>" name="tag_title" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Latitude / Longitude - Mapa</label>
      <div class="g-pos-rel">
        <input type="text" value="<?php echo $rows['latlon'] ?>" name="latlon" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Descrição para o Google</label>
      <div class="g-pos-rel">
        <input type="text" value="<?php echo $rows['tag_description'] ?>" name="tag_description" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Palavras-chave para o Google</label>
      <div class="g-pos-rel">
        <input type="text" value="<?php echo $rows['tag_keywords'] ?>" name="tag_keywords" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>

  </div>
</div>

<div class="col-md-12">
  <div class="g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

    <h4 class="d-flex align-self-center text-uppercase g-font-weight-300 g-color-black g-mb-20">Layout</h4>

    <div class="form-group">
      <h4 class="h6 g-font-weight-600 g-color-black g-mb-20">Ícone do site</h4>
      <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
        <input class="form-control form-control-md rounded-0" placeholder="<?php echo $rows['favicon'] ?>" readonly="" type="text">
        <div class="input-group-btn">
          <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
          <input type="file" name="favicon">
        </div>
      </div>
      <?php if($rows['favicon']){ ?>
        <img src="<?php echo base_url() .'uploads/config/'. $rows['favicon'] ?>" height="45" vspace="10"><br>
      <?php } ?>
    </div>

    <div class="form-group">
      <h4 class="h6 g-font-weight-600 g-color-black g-mb-20">Logo Principal</h4>
      <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
        <input class="form-control form-control-md rounded-0" placeholder="<?php echo $rows['logo_principal'] ?>" readonly="" type="text">
        <div class="input-group-btn">
          <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
          <input type="file" name="logo_principal">
        </div>
      </div>
      <?php if($rows['logo_principal']){ ?>
        <img src="<?php echo base_url() .'uploads/config/'. $rows['logo_principal'] ?>" height="45" vspace="10"><br>
      <?php } ?>
    </div>

    <div class="form-group">
      <h4 class="h6 g-font-weight-600 g-color-black g-mb-20">Logo Secundária</h4>
      <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
        <input class="form-control form-control-md rounded-0" placeholder="<?php echo $rows['logo_secundaria'] ?>" readonly="" type="text">
        <div class="input-group-btn">
          <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
          <input type="file" name="logo_secundaria">
        </div>
      </div>
      <?php if($rows['logo_secundaria']){ ?>
        <img src="<?php echo base_url() .'uploads/config/'. $rows['logo_secundaria'] ?>" height="45" vspace="10"><br>
      <?php } ?>
    </div>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Cor principal</label>
      <div class="g-pos-rel">
        <input type="color" name="cor_principal" value="<?php echo $rows['cor_principal'] ?>" class="w-100 g-pa-0 g-height-50 g-brd-none">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Cor rodapé</label>
      <div class="g-pos-rel">
        <input type="color" name="cor_rodape" value="<?php echo $rows['cor_rodape'] ?>" class="w-100 g-pa-0 g-height-50 g-brd-none">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Cor ícones</label>
      <div class="g-pos-rel">
        <input type="color" name="cor_rodape_icones_fonte" value="<?php echo $rows['cor_rodape_icones_fonte'] ?>" class="w-100 g-pa-0 g-height-50 g-brd-none">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Cor ícones fundo</label>
      <div class="g-pos-rel">
        <input type="color" name="cor_rodape_icones_fundo" value="<?php echo $rows['cor_rodape_icones_fundo'] ?>" class="w-100 g-pa-0 g-height-50 g-brd-none">
      </div>
    </div>


  </div>
</div>

<div class="col-md-12">
  <div class="g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

    <h4 class="d-flex align-self-center text-uppercase g-font-weight-300 g-color-black g-mb-20">E-mail</h4>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Endereço SMTP</label>
      <div class="g-pos-rel">
        <input type="text" name="smtp_host" value="<?php echo $rows['smtp_host'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Porta do SMTP</label>
      <div class="g-pos-rel">
        <input type="text" name="smtp_host" value="<?php echo $rows['smtp_host'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">E-mail principal</label>
      <div class="g-pos-rel">
        <input type="text" name="smtp_user" value="<?php echo $rows['smtp_user'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Senha do e-mail</label>
      <div class="g-pos-rel">
        <input type="text" name="smtp_pass" value="<?php echo $rows['smtp_pass'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>


  </div>
</div>

<div class="col-md-12">
  <div class="g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

    <h4 class="d-flex align-self-center text-uppercase g-font-weight-300 g-color-black g-mb-20">Redes Sociais</h4>

    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Facebook</label>
      <div class="g-pos-rel">
        <input type="text" name="facebook" value="<?php echo $rows['facebook'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Instagram</label>
      <div class="g-pos-rel">
        <input type="text" name="instagram"  value="<?php echo $rows['instagram'] ?>"class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">Twitter</label>
      <div class="g-pos-rel">
        <input type="text" name="twitter" value="<?php echo $rows['twitter'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">YouTube</label>
      <div class="g-pos-rel">
        <input type="text" name="youtube" value="<?php echo $rows['youtube'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>
    <div class="form-group g-mb-30">
      <label class="g-mb-10" for="inputGroup-1_1">LinkedIn</label>
      <div class="g-pos-rel">
        <input type="text" name="linkedin" value="<?php echo $rows['linkedin'] ?>" class="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10">
      </div>
    </div>

  </div>
</div>