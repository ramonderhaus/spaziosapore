<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="titulo_site">Título do Site</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="titulo_site" id="titulo_site" value="<?php echo $rows['tag_title'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="description">Descrição para o Google</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="description" id="description" value="<?php echo $rows['tag_description'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

        <div class="col-md-4 form-group g-mb-30">
          <label class="g-mb-10" for="keywords">Palavras-chave para o Google</label>
          <div class="g-pos-rel">
            <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
            <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
          </span>
            <input name="keywords" id="keywords" value="<?php echo $rows['tag_keywords'] ?>" type="text" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" required>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="form-group col-md-4">
          <img src="<?php echo base_url() . 'uploads/config/' . $rows['favicon'] ?>" height="45" vspace="10"><br>
          <label>Ícone do site</label>
          <input type="file" class="form-control" placeholder="Ícone do site" value="<?php echo $rows['favicon'] ?>" name="favicon">
        </div>
        <div class="form-group col-md-4">
          <img src="<?php echo base_url() . 'uploads/config/' . $rows['logo_principal'] ?>" height="45" vspace="10"><br>
          <label>Logo Principal</label>
          <input type="file" class="form-control" placeholder="Logo Principal" value="<?php echo $rows['logo_principal'] ?>" name="logo_principal">
        </div>
        <div class="form-group col-md-4">
          <img src="<?php echo base_url() . 'uploads/config/' . $rows['logo_secundaria'] ?>" height="45" vspace="10"><br>
          <label>Logo Secundária</label>
          <input type="file" class="form-control" placeholder="Logo Secundária" value="<?php echo $rows['logo_secundaria'] ?>" name="logo_secundaria">
        </div>
        <div class="form-group col-md-3">
          <label>Cor principal</label>
          <input type="color" class="form-control" value="<?php echo $rows['cor_principal'] ?>" name="cor_principal">
        </div>
        <div class="form-group col-md-3">
          <label>Cor rodapé</label>
          <input type="color" class="form-control" value="<?php echo $rows['cor_rodape'] ?>" name="cor_rodape">
        </div>
        <div class="form-group col-md-3">
          <label>Cor ícones</label>
          <input type="color" class="form-control" value="<?php echo $rows['cor_rodape_icones_fonte'] ?>" name="cor_rodape_icones_fonte">
        </div>
        <div class="form-group col-md-3">
          <label>Cor ícones fundo</label>
          <input type="color" class="form-control" value="<?php echo $rows['cor_rodape_icones_fundo'] ?>" name="cor_rodape_icones_fundo">
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-6">
          <label>Endereço SMTP</label>
          <input type="text" class="form-control" placeholder="Endereço SMTP" value="<?php echo $rows['smtp_host'] ?>" name="smtp_host">
        </div>
        <div class="form-group col-md-6">
          <label>Porta do SMTP</label>
          <input type="text" class="form-control" placeholder="Porta do SMTP" value="<?php echo $rows['smtp_port'] ?>" name="smtp_port">
        </div>
        <div class="form-group col-md-6">
          <label>E-mail principal</label>
          <input type="text" class="form-control" placeholder="E-mail principal" value="<?php echo $rows['smtp_user'] ?>" name="smtp_user">
        </div>
        <div class="form-group col-md-6">
          <label>Senha do e-mail</label>
          <input type="password" class="form-control" placeholder="Senha do e-mail" value="<?php echo $rows['smtp_pass'] ?>" name="smtp_pass">
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-2">
          <label>Item 1 - Ícone</label>
          <input type="text" class="form-control" placeholder="Item 1 - Ícone" value="<?php echo $rows['contato_icone1'] ?>" name="contato_icone1">
        </div>
        <div class="form-group col-md-5">
          <label>Item 1 - Título</label>
          <input type="text" class="form-control" placeholder="Item 1 - Título" value="<?php echo $rows['contato_titulo1'] ?>" name="contato_titulo1">
        </div>
        <div class="form-group col-md-5">
          <label>Item 1 - Texto</label>
          <input type="text" class="form-control" placeholder="Item 1 - Texto" value="<?php echo $rows['contato_texto1'] ?>" name="contato_texto1">
        </div>
        <div class="form-group col-md-2">
          <label>Item 2 - Ícone</label>
          <input type="text" class="form-control" placeholder="Item 2 - Ícone" value="<?php echo $rows['contato_icone2'] ?>" name="contato_icone2">
        </div>
        <div class="form-group col-md-5">
          <label>Item 2 - Título</label>
          <input type="text" class="form-control" placeholder="Item 2 - Título" value="<?php echo $rows['contato_titulo2'] ?>" name="contato_titulo2">
        </div>
        <div class="form-group col-md-5">
          <label>Item 2 - Texto</label>
          <input type="text" class="form-control" placeholder="Item 2 - Texto" value="<?php echo $rows['contato_texto2'] ?>" name="contato_texto2">
        </div>
        <div class="form-group col-md-2">
          <label>Item 3 - Ícone</label>
          <input type="text" class="form-control" placeholder="Item 3 - Ícone" value="<?php echo $rows['contato_icone3'] ?>" name="contato_icone3">
        </div>
        <div class="form-group col-md-5">
          <label>Item 3 - Título</label>
          <input type="text" class="form-control" placeholder="Item 3 - Título" value="<?php echo $rows['contato_titulo3'] ?>" name="contato_titulo3">
        </div>
        <div class="form-group col-md-5">
          <label>Item 3 - Texto</label>
          <input type="text" class="form-control" placeholder="Item 3 - Texto" value="<?php echo $rows['contato_texto3'] ?>" name="contato_texto3">
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-6">
          <label>Facebook</label>
          <input type="text" class="form-control" placeholder="Facebook" value="<?php echo $rows['facebook'] ?>" name="facebook">
        </div>
        <div class="form-group col-md-6">
          <label>Instagram</label>
          <input type="text" class="form-control" placeholder="Instagram" value="<?php echo $rows['instagram'] ?>" name="instagram">
        </div>
        <div class="form-group col-md-4">
          <label>Twitter</label>
          <input type="text" class="form-control" placeholder="Twitter" value="<?php echo $rows['twitter'] ?>" name="twitter">
        </div>
        <div class="form-group col-md-4">
          <label>YouTube</label>
          <input type="text" class="form-control" placeholder="YouTube" value="<?php echo $rows['youtube'] ?>" name="youtube">
        </div>
        <div class="form-group col-md-4">
          <label>LinkedIn</label>
          <input type="text" class="form-control" placeholder="LinkedIn" value="<?php echo $rows['linkedin'] ?>" name="linkedin">
        </div>
      </div>

      </div>
    </div>
  </div>
</div>