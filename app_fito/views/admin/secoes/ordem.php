<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
	<h1 class="g-font-weight-300 g-color-primary g-mb-28"><?php echo $tag_title ?></h1>
	<div class="row">
		<div class="col-md-12">
			<div class="g-brd-around g-brd-gray-light-v3 g-mb-40 g-pa-20">
				<?php echo form_open('admin/secoes/ordenar/') ?>
				<h5 class="g-mb-20">Clique e arraste para definir a ordem das seções:</h5>
				<div class="sortable">
				<?php if ($rows) { ?>
				<?php foreach ($rows as $row) { ?>
				<!-- Panel -->
				  <header class="card-header g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v3 g-rounded-50 g-pa-10-15 g-bg-gray-light-v3--hover g-mb-5">
					<div class="media">
					  <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mr-10 mb-0"><i class="hs-admin-exchange-vertical g-mr-10"></i> <?php echo $row['tag'] ?></h3>
					  <input name="ordem[]" type="hidden" value="<?php echo $row['id'] ?>" />
					</div>
				  </header>
                <!-- End Panel -->
				<?php } ?>
				<?php } ?>
				</div>
				<button class="btn u-btn-outline-primary g-mt-10" type="submit">
					<i class="hs-admin-check g-font-size-18"></i>
					<span class="g-ml-10">Confirmar</span>
				</button>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>