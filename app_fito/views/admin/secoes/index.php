<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
	<h1 class="g-font-weight-300 g-color-primary g-mb-28">
		<?php echo $tag_title ?>
	</h1>
	<div class="row">
		<div class="col-md-12">
			<div class="paginas">
				<div id="accordion-07" class="u-accordion u-accordion-color-primary" role="tablist" aria-multiselectable="true">
					
					<?php $i = 1; foreach ($paginas as $pagina => $ids) { ?>
					<div class="card g-rounded-50 mb-1 g-brd-gray-light-v3 g-bg-gray-light-v4--hover g-transition-0_3">
						<div id="accordion-07-heading-0<?php echo $i; ?>" class="u-accordion__header g-pa-0" role="tab">
							<h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700">
								<a class="collapsed d-flex g-color-main g-text-underline--none--hover" href="#accordion-07-body-0<?php echo $i; ?>" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="false" aria-controls="accordion-07-body-0<?php echo $i; ?>">
									<span class="u-accordion__control-icon g-brd-right g-brd-gray-light-v3 g-color-primary text-center g-pa-20">
										<i class="fa fa-plus"></i>
										<i class="fa fa-minus"></i>
									</span>
									<span class="g-pa-20"><?php echo $pagina; ?></span>
								</a>
							</h5>
						</div>
						<div id="accordion-07-body-0<?php echo $i; ?>" class="collapse" role="tabpanel" aria-labelledby="accordion-07-heading-0<?php echo $i; ?>" data-parent="#accordion-07">
							<div class="u-accordion__body g-bg-gray-light-v5 g-p-20 g-pb-30" style="border-radius: 0 0 50px 50px;">
								<?php
								foreach ($ids as $id) {
									$bg = !($id % 2) ? "g-bg-gray-light-v5" : "g-bg-white";
									$checked = $rows[$id - 1]['ativo'] == 1 ? ' checked' : '';
									?>
									<div class="d-flex flex-column flex-md-row align-items-end align-items-md-center <?php echo $bg; ?> p-3 g-bg-gray-light-v4--hover g-transition-0_3">
										<div class="mr-auto"><?php echo $rows[$id - 1]['tag']; ?></div>
										<div class="d-flex align-items-center">
											<label class="form-check-inline u-check g-mr-20 mx-0 mb-0">
												<input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" id="ativo<?php echo $rows[$id - 1]['id']; ?>" <?php echo $checked; ?> type="checkbox">
												<div class="u-check-icon-radio-v7">
													<i class="fa ativo" data-check-icon="&#xf00c" data-uncheck-icon="&#xf00d" data-id="<?php echo $rows[$id - 1]['id']; ?>" data-table="secoes"></i>
												</div>
											</label>
											<div>
												<a id="submenu<?php echo $rows[$id - 1]['id'] ?>" class="u-link-v5 g-line-height-0 g-font-size-24 g-color-gray-light-v6 g-color-black--hover" href="#!" aria-controls="dropDown<?php echo $rows[$id - 1]['id'] ?>" aria-haspopup="true" aria-expanded="false" data-dropdown-event="click" data-dropdown-target="#dropDown<?php echo $rows[$id - 1]['id'] ?>"><i class="hs-admin-align-justify"></i></a>								

												<div id="dropDown<?php echo $rows[$id - 1]['id'] ?>" class="u-shadow-v31 g-pos-abs g-right-0 g-z-index-2 g-bg-white" aria-labelledby="submenu<?php echo $rows[$id - 1]['id'] ?>">
													<ul class="list-unstyled g-nowrap mb-0">
														<?php if ($rows[$id - 1]['id'] == 2) { ?>
														<li>
															<a href="<?php echo base_url() ?>admin/pagina/home" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14"><i class="hs-admin-archive g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Itens da seção</a>
														</li>
														<?php } ?>
														<?php if ($rows[$id - 1]['id'] == 12 || $rows[$id - 1]['id'] == 13) { ?>
														<li>
															<a href="<?php echo base_url() ?>admin/componente1/form/<?php echo $id - 4 ?>" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14"><i class="hs-admin-archive g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Itens da seção</a>
														</li>
														<?php } ?>
														<li>
															<a href="<?php echo base_url() ?>admin/secoes/form/<?php echo $rows[$id - 1]['id'] ?>" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14"><i class="hs-admin-pencil g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Editar</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					
					<?php $i++; } ?>
					
				</div>
			</div>
		</div>
	</div>
</div>