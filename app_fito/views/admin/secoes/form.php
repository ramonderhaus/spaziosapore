<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">
        <div class="form-group col-sm-12 g-mb-30">
          <label class="g-mb-10">Tag</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Tag" value="<?php echo $row["tag"]; ?>" name="tag" id="tag">
        </div>
        <div class="form-group col-md-8 col-lg-10">
          <label class="g-mb-10">Título da seção</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título da seção" value="<?php echo $row["titulo"]; ?>" name="titulo" id="titulo">
        </div>
        <div class="form-group col-md-4 col-lg-2">
          <label class="g-mb-10">Cor do texto</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["titulo_cor"]; ?>" name="titulo_cor" id="titulo_cor">
        </div>
        <div class="linha"></div>
        <div class="form-group col-md-8 col-lg-10">
          <label class="g-mb-10">Descrição da seção</label>
          <textarea class="form-control form-control-md tinymce g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" rows="7" placeholder="Subtítulo da seção" name="subtitulo" id="subtitulo"><?php echo $row["subtitulo"]; ?></textarea>
        </div>
        <div class="form-group col-md-4 col-lg-2">
          <label class="g-mb-10">Cor do texto</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["subtitulo_cor"]; ?>" name="subtitulo_cor" id="subtitulo_cor">
        </div>
        <div class="linha"></div>
        <div class="form-group col-md-6 col-lg-3">
          <label class="g-mb-10">Qtde da cor ( <strong><span id="tf"><?php echo ($row["transparencia_fundo"]*10); ?>%</span></strong> )</label>
          <input type="range" min="0" max="10" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["transparencia_fundo"]; ?>" name="transparencia_fundo" id="transparencia_fundo" onChange="$('#tf').html(((this.value)*10)+'%')">
        </div>
        <div class="form-group col-6 col-md-3 col-lg-2">
          <label class="g-mb-10">Cor de fundo</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["cor_fundo"]; ?>" name="cor_fundo" id="cor_fundo">
        </div>
        <div class="form-group col-6 col-md-3 col-lg-2">
          <label class="g-mb-10">Cor de fundo2</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["cor_fundo2"]; ?>" name="cor_fundo2" id="cor_fundo2">
        </div>
        <div class="form-group col-sm-12 col-lg-5">
          <label class="g-mb-10">Foto de fundo ( <?php echo ($row["foto_fundo"] == "") ? "<em style='color: #ccc'>sem foto</em>" : "<a href='javascript:(void);' data-toggle='modal' data-target='#myModal2'>apagar foto</a>"; ?> )</label>
          <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
            <input class="form-control form-control-md rounded-0 g-bg-white" placeholder="Procurar arquivo" readonly="" type="text">
            <div class="input-group-btn">
              <button class="btn btn-md h-100 u-btn-primary rounded-0" type="submit">Procurar</button>
              <input name="foto_fundo" id="foto_fundo" type="file">
            </div>
          </div>
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Apagar foto</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                  <img src="<?php echo base_url(); ?>uploads/site/secao/<?php echo $row["id"]; ?>/<?php echo $row["foto_fundo"]; ?>" class="img-fluid">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <a href="<?php echo base_url() ?>admin/secoes/remover_imagem/<?php echo $row['id'] ?>" class="btn btn-primary remove">Apagar</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="linha"></div>
        <div class="form-group col-12">
          <label class="g-mb-10"><strong>CONFIGURAÇÃO DOS COMPONENTES</strong></label>
        </div>
        <div class="form-group col-6 col-md-3">
          <label class="g-mb-10">Componente Título</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["componente_titulo"]; ?>" name="componente_titulo" id="componente_titulo">
        </div>
        <div class="form-group col-6 col-md-3">
          <label class="g-mb-10">Componente Subtítulo</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["componente_subtitulo"]; ?>" name="componente_subtitulo" id="componente_subtitulo">
        </div>
        <div class="form-group col-6 col-md-3">
          <label class="g-mb-10">Componente Fundo 1</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["componente_fundo1"]; ?>" name="componente_fundo1" id="componente_fundo1">
        </div>
        <div class="form-group col-6 col-md-3">
          <label class="g-mb-10">Componente Fundo 2</label>
          <input type="color" class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" value="<?php echo $row["componente_fundo2"]; ?>" name="componente_fundo2" id="componente_fundo2">
        </div>
      </div>
    </div>
  </div>
</div>