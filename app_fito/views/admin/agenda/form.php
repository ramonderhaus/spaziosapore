<div class="g-pa-20">
  <div class="row">
    <div class="col-md-12 g-bg-gray-light-v4 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
      <div class="row">
        <div class="form-group col-md-4">
          <label class="g-mb-10">Data</label>
					<div id="datepickerWrapper" class="g-bg-white u-datepicker-right u-datepicker--v3 g-pos-rel w-100 g-cursor-pointer g-brd-around g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4">
						<input class="js-range-datepicker g-bg-transparent g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-pr-80 g-pl-15 g-py-9" type="date" name="data" placeholder="<?php echo $data ? $data : 'Selecione a data'; ?>" data-rp-wrapper="#datepickerWrapper" data-rp-date-format="d/m/Y" value="<?php echo $data; ?>">
						<div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
							<i class="hs-admin-calendar g-font-size-18 g-mr-10"></i>
							<i class="hs-admin-angle-down"></i>
						</div>
					</div>
				</div>
        <div class="form-group col-md-12">
          <label class="g-mb-10">Titulo</label>
          <input class="form-control form-control-md g-brd-gray-light-v3 g-brd-gray-light-v1--focus g-rounded-4 g-px-14 g-py-10" placeholder="Título" value="<?php echo $row["titulo"]; ?>" name="titulo" id="titulo">
        </div>
      </div>
    </div>
  </div>
</div>