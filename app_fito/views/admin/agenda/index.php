
<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
  <h1 class="g-font-weight-300 g-color-primary g-mb-28"><?php echo $tag_title ?></h1>
  <div class="row">
    <div class="col-md-12">
      <div class="media-md align-items-center g-mb-30">
        <div class="d-flex">
          <h3 class="g-font-weight-400 g-font-size-16 g-color-black mb-0"><a href="<?php echo base_url() ?>admin/agenda/form" class="btn u-btn-primary g-mr-10">Cadastrar <i class="hs-admin-plus"></i></a></h3>
        </div>
      </div>
      <div class="table-responsive g-mb-40">
        <table class="js-datatable table table-hover table-bordered g-color-black" data-order='[[ 0, "asc" ]]'>
          <thead>
          <tr>
            <th class="g-width-150">Data</th>
            <th>Título</th>
            <th class="g-width-120">Ações</th>
          </tr>
          </thead>
          <tbody>
          <?php if ($rows) { ?>
            <?php foreach ($rows as $row) { ?>
              <tr>
                <td data-order="<?php echo strtotime($row['data']); ?>"><?php echo date('d/m/Y', strtotime($row['data'])); ?></td>
                <td><?php echo $row['titulo'] ?></td>
                <td class="d-flex align-items-center justify-content-center">
									<label class="form-check-inline u-check g-mr-10 mx-0 mb-0">
										<input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" id="ativo<?php echo $row['id'] ?>" <?php echo ($row['ativo'] == 1) ? 'checked' : '' ?> type="checkbox">
										<div class="u-check-icon-radio-v7">
											<i class="fa ativo" data-check-icon="&#xf00c" data-uncheck-icon="&#xf00d" data-id="<?php echo $row['id'] ?>" data-table="agenda"></i>
										</div>
									</label>
									<div class="g-mr-10">
										<a href="<?php echo base_url() ?>admin/agenda/form/<?php echo $row['id'] ?>" class="btn u-btn-outline-blue rounded-circle" data-toggle="tooltip" data-placement="top" title="Editar">
											<i class="fa fa-edit" aria-hidden="true"></i>
										</a>
									</div>
									<div>
										<a href="<?php echo base_url() ?>admin/agenda/deletar/<?php echo $row['id'] ?>" class="btn btn-outline-danger rounded-circle remove" data-toggle="tooltip" data-placement="top" title="Excluir">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</a>
									</div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
          </tbody>
        </table>
      </div>
    
    </div>
  </div>
</div>