<?php $this->load->view('admin/template/breadcrumb'); ?>

<div class="g-pa-20">
  <h1 class="g-font-weight-300 g-color-primary g-mb-28"><?php echo $tag_title ?></h1>
  <div class="row">
    <div class="col-md-12">
      <div class="media-md align-items-center g-mb-30">
        <div class="d-flex">
          <h3 class="g-font-weight-400 g-font-size-16 g-color-black mb-0"><a href="<?php echo base_url() ?>admin/clientes/form" class="btn u-btn-primary g-mr-10">Cadastrar <i class="hs-admin-plus"></i></a></h3>
        </div>
      </div>
      <div class="table-responsive g-mb-40">
        <table class="js-datatable table table-hover table-bordered g-color-black">
          <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Documento</th>
              <th>WhatsApp</th>
              <th>Categoria</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($rows) { ?>
            <?php foreach ($rows as $row) { ?>
              <tr>
                <td class="text-right text-md-center"><?php echo $row['id'] ?></td>
                <td><?php echo $row['nome'] ?></td>
                <td><?php echo $row['email'] ?></td>
                <td><?php echo $row['cpf_cnpj'] ?></td>
                <td><?php echo $row['whatsapp'] ?></td>
                <td class="g-px-30">
                  <div class="d-inline-block">
                    <span class="d-flex align-items-center justify-content-center u-tags-v1 g-brd-around g-bg-gray-light-v8 g-bg-gray-light-v8 g-font-size-default g-color-gray-dark-v6 g-rounded-50 g-py-4 g-px-15">
                      <?php if ( strlen($row['cpf_cnpj']) == 14 ) {  ?>
                        <span class="u-badge-v2--md g-pos-stc g-transform-origin--top-left g-bg-lightblue-v3 g-mr-8"></span>Pessoa Física
                      <?php } else if ( strlen($row['cpf_cnpj']) == 18 ) {  ?>
                        <span class="u-badge-v2--md g-pos-stc g-transform-origin--top-left g-bg-primary g-mr-8"></span>Pessoa Jurídica
                      <?php } ?>
                    </span>
                  </div>
                </td>
                <td align="center">
                  <div class="g-pos-rel g-top-3 d-inline-block">
                    <a id="submenu<?php echo $row['id'] ?>" class="u-link-v5 g-line-height-0 g-font-size-24 g-color-gray-light-v6 g-color-lightblue-v3--hover" href="#!" aria-controls="dropDown<?php echo $row['id'] ?>" aria-haspopup="true" aria-expanded="false" data-dropdown-event="click" data-dropdown-target="#dropDown<?php echo $row['id'] ?>">
                      <i class="hs-admin-align-justify"></i>
                    </a>

                    <div id="dropDown<?php echo $row['id'] ?>" class="u-shadow-v31 g-pos-abs g-right-0 g-z-index-2 g-bg-white" aria-labelledby="submenu<?php echo $row['id'] ?>">
                      <ul class="list-unstyled g-nowrap mb-0">
                        <li>
                          <a href="<?php echo base_url() ?>admin/clientes/form/<?php echo $row['id'] ?>" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14">
                            <i class="hs-admin-pencil g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Editar
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo base_url() ?>admin/clientes/deletar/<?php echo $row['id'] ?>" class="d-flex align-items-center u-link-v5 g-bg-gray-light-v8--hover g-font-size-12 g-font-size-default--md g-color-gray-dark-v6 g-px-25 g-py-14 remove">
                            <i class="hs-admin-trash g-font-size-18 g-color-gray-dark-v6 g-mr-10 g-mr-15--md"></i> Deletar
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>