<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MY_Controller extends CI_Controller {

  public $data = array();

  public function __construct () {
    parent::__construct();
  
    $this->load->model('agenda_model');
    $this->load->model('marcas_model');
  
    $this->load->model('componente1_model');
    $this->load->model('componente2_model');
    $this->load->model('componente3_model');
    $this->load->model('componente4_model');
    $this->load->model('componente5_model');
    $this->load->model('componente6_model');

    $this->load->model('secoes_model');
    $this->load->model('clientes_model');
    $this->load->model('textos_model');
    $this->load->model('settings_model');
    $this->load->model('users_model');

    $this->load->helper('form');
    $this->load->helper('upload');
    $this->load->library('form_validation');
    $this->load->library('user_agent');

    $this->data['styles'] = $this->secoes_model->order_by('titulo')->get_all();

    $config = $this->settings_model->get_all();
    $this->config->load('config');
    foreach ($config as $item) {
      $this->config->set_item($item['campo'], $item['valor']);
      $this->data[$item['campo']] = $item['valor'];
    }

    $textos = $this->textos_model->get_by('pt_br');
    foreach ($textos as $item) {
      $this->data['txt'][$item['secao']][$item['campo']] = $item['valor'];
    }

    $this->data['mobile'] = $this->agent->is_mobile();
    $this->data['uploads_url'] = 'uploads/';
  }
	
	public function feedInstagram() {		
		$access_token = '9739229608.d71ffcd.978cef8822594e138bf6cbd2838e5e26';
		$user = file_get_contents( "https://api.instagram.com/v1/users/self/media/recent/?access_token=$access_token" );
		$this->data['feedInstagram'] = json_decode($user, true)['data'];
	}

  public function debug($var) {
    echo '<pre>'; var_dump($var); die;
  }

  public function load_page ( $modulo, $pagina, $opcoes = array() ) {

    // Aplica opções de layout
    $default = array( 'header' => 'header', 'menu' => 'menu', 'footer' => 'footer');
    $configs = array_merge($default, $opcoes);

    if($configs['header']){ $this->load->view( $modulo . '/template/' . $configs['header'], $this->data ); }
    if($configs['menu']){ $this->load->view( $modulo . '/template/' . $configs['menu'] ); }
    $this->load->view( $modulo . '/' . $pagina );
    if($configs['footer']){ $this->load->view( $modulo . '/template/' . $configs['footer'] ); }
  }

  public function load_form ( $pagina, $form = '', $parametro = '', $modulo = 'admin', $arquivo = 'form' ) {

    $this->data['pagina']    = $pagina . '/' . $arquivo;
    $this->data['form']      = $form;
    $this->data['parametro'] = $parametro;

    $this->load_page($modulo, 'template/form');
  }

	public function inserirSM ( $title = '', $url = '', $image = '', $description = '', $keywords = '' ) {
		$this->data['sm']['title']		 = ($title) ? $title : $this->config->item('tag_title');
		$this->data['sm']['url']		 = base_url() . $url;
		$this->data['sm']['image']		 = ($image) ? base_url() . $image : base_url() . 'assets/images/favicon.png';
		$this->data['sm']['description'] = ($description) ? $description : $this->config->item('tag_description');
		$this->data['sm']['keywords'] = ($keywords) ? $keywords : $this->config->item('tag_keywords');
	}

  public function set_tags ( $title = '', $keywords = '', $description = '', $canonical = '' ) {
    $this->data['tag_title']       = ($title) ? $title : $this->config->item('tag_title');
    $this->data['tag_keywords']    = ($keywords) ? $keywords : $this->config->item('tag_keywords');
    $this->data['tag_description'] = ($description) ? $description : $this->config->item('tag_description');
    $this->data['tag_canonical']   = $this->config->item('base_url') . $canonical;
  }
	
  public function auth_admin () {
    if ($this->session->userdata('admin_logado') == FALSE) {

      $form_email = $this->input->post('email', TRUE);
      $form_senha = $this->input->post('senha', TRUE);

      $usuario = $this->users_model->get_cliente($form_email, $form_senha);

      if ($usuario) {
        $dadosSessao['admin_id']     = $usuario->id;
        $dadosSessao['admin_nome']   = $usuario->nome;
        $dadosSessao['admin_logado'] = TRUE;
        $this->session->set_userdata($dadosSessao);
        redirect( 'admin/secoes', 'location' );
      } else {
        $dadosSessao['admin_id']     = NULL;
        $dadosSessao['admin_nome']   = NULL;
        $dadosSessao['admin_logado'] = FALSE;
        $dadosSessao['alert_class']  = 'danger';
        $dadosSessao['alert_msg']    = 'Usuário e/ou senha incorreto!';
        $this->session->set_userdata($dadosSessao);
        redirect( 'admin/', 'location' );
      }
    }
  }

  public function auth_cliente () {

    if ($this->session->userdata('cliente_logado') == FALSE) {

      $form_email = 'william@fitosistemas.com.br';
      $form_senha = 'teste';

      $this->db->where('email', $form_email);
      $this->db->where('senha', $form_senha);
      $this->db->where('deleted_at', '0000-00-00 00:00:00');

      $usuario = $this->db->get('clientes')->row();

      if ( isset($usuario->email) and $usuario->email != '' ) {
        $dadosSessao['cliente_id']     = $usuario->id;
        $dadosSessao['cliente_nome']   = $usuario->nome;
        $dadosSessao['cliente_logado'] = TRUE;
        $this->session->set_userdata($dadosSessao);
        redirect( 'cliente/dashboard', 'location' );
      } else {
        $dadosSessao['cliente_id']     = NULL;
        $dadosSessao['cliente_nome']   = NULL;
        $dadosSessao['cliente_logado'] = FALSE;
        $dadosSessao['class']          = 'danger';
        $dadosSessao['resposta']       = 'Usuário e/ou senha incorreto!';
        $this->session->set_userdata($dadosSessao);
        redirect( 'cliente/', 'location' );
      }
    }
  }

  public function logoff_admin () {
    $dadosSessao['admin_id']     = NULL;
    $dadosSessao['admin_nome']   = NULL;
    $dadosSessao['admin_logado'] = FALSE;
    $dadosSessao['alert_class']  = 'success';
    $dadosSessao['alert_msg']    = 'Você saiu com sucesso!';
    $this->session->set_userdata($dadosSessao);
    redirect('admin/login', 'location');
  }
	
  public function set_asset ( $tipo, $arquivo ) {
    $this->config->load('config');
    $recurso = $this->config->item($tipo);
    $recurso[] = $arquivo;
    $this->config->set_item($tipo, $recurso);
  }

  public function set_alert($classe, $mensagem){
    $this->data['classe']   = $classe;
    $this->data['resposta'] = $mensagem;
  }

  public function ativo() {
    if ( $this->input->post() ) {
      $data           = $this->input->post();
      $id             = $data['id'];
      $table          = $data['table'] . '_model';
      $dados['ativo'] = $data['ativo'];

      $this->{$table}->update( $dados, $id );
    }
  }

  public function load_recursos($pagina){

    switch ($pagina) {
      case 'SITE':

      $this->data['assets_css'][] = 'libraries/bootstrap/bootstrap.min';
      $this->data['assets_css'][] = 'libraries/icon-awesome/css/font-awesome.min';
      $this->data['assets_css'][] = 'libraries/icon-line/css/simple-line-icons';
      $this->data['assets_css'][] = 'libraries/icon-hs/style';
      $this->data['assets_css'][] = 'libraries/hamburgers/hamburgers.min';
      $this->data['assets_css'][] = 'libraries/dzsparallaxer/dzsparallaxer';
      $this->data['assets_css'][] = 'libraries/dzsparallaxer/dzsscroller/scroller';
      $this->data['assets_css'][] = 'libraries/dzsparallaxer/advancedscroller/plugin';	
      $this->data['assets_css'][] = 'styles/animate';
      $this->data['assets_css'][] = 'libraries/slick-carousel/slick/slick';
      $this->data['assets_css'][] = 'libraries/hs-bg-video/hs-bg-video';
      $this->data['assets_css'][] = 'styles/style';
      $this->data['assets_css'][] = 'styles/secoes';
      $this->data['assets_css'][] = 'styles/custom';
      $this->data['assets_js'][] = 'scripts/less.min';
      $this->data['assets_js'][] = 'libraries/jquery/jquery.min';
      $this->data['assets_js'][] = 'libraries/jquery-migrate/jquery-migrate.min';
      $this->data['assets_js'][] = 'scripts/popper.min';
      $this->data['assets_js'][] = 'libraries/bootstrap/bootstrap.min';
      $this->data['assets_js'][] = 'scripts/appear';
      $this->data['assets_js'][] = 'libraries/dzsparallaxer/dzsparallaxer';
      $this->data['assets_js'][] = 'libraries/dzsparallaxer/dzsscroller/scroller';
      $this->data['assets_js'][] = 'libraries/dzsparallaxer/advancedscroller/plugin';
      $this->data['assets_js'][] = 'libraries/hs-bg-video/hs-bg-video';
      $this->data['assets_js'][] = 'libraries/hs-bg-video/vendor/player.min';
      $this->data['assets_js'][] = 'libraries/slick-carousel/slick/slick';
      $this->data['assets_js'][] = 'scripts/hs.core';
      $this->data['assets_js'][] = 'libraries/components/hs.header';
      $this->data['assets_js'][] = 'libraries/components/hs.header-fullscreen';
      $this->data['assets_js'][] = 'libraries/helpers/hs.hamburgers';
      $this->data['assets_js'][] = 'libraries/components/hs.scroll-nav';
      $this->data['assets_js'][] = 'libraries/components/hs.onscroll-animation';
      $this->data['assets_js'][] = 'libraries/components/hs.tabs';
      $this->data['assets_js'][] = 'libraries/components/hs.carousel';
      $this->data['assets_js'][] = 'libraries/components/hs.go-to';
      $this->data['assets_js'][] = 'libraries/components/hs.count-qty';
      $this->data['assets_js'][] = 'libraries/hs-bg-video/helper/hs.bg-video';
      $this->data['assets_js'][] = 'scripts/custom';

      break;

      case 'ADMIN':
      $this->data['assets_css'][] = 'libraries/bootstrap/bootstrap.min';
      $this->data['assets_css'][] = 'libraries/icon-awesome/css/font-awesome.min';
      $this->data['assets_css'][] = 'libraries/icon-line/css/simple-line-icons';
      $this->data['assets_css'][] = 'libraries/icon-etlinefont/style';
      $this->data['assets_css'][] = 'libraries/icon-line-pro/style';
      $this->data['assets_css'][] = 'libraries/icon-hs/style';
      $this->data['assets_css'][] = 'libraries/hs-admin-icons/hs-admin-icons';
      $this->data['assets_css'][] = 'libraries/animate';
      $this->data['assets_css'][] = 'libraries/malihu-scrollbar/jquery.mCustomScrollbar.min';
      $this->data['assets_css'][] = 'libraries/flatpickr/dist/css/flatpickr.min';
      $this->data['assets_css'][] = 'libraries/bootstrap-select/css/bootstrap-select.min';
      $this->data['assets_css'][] = 'libraries/bootstrap-tagsinput/css/bootstrap-tagsinput';
      $this->data['assets_css'][] = 'libraries/chartist-js/chartist.min';
      $this->data['assets_css'][] = 'libraries/chartist-js-tooltip/chartist-plugin-tooltip';
      $this->data['assets_css'][] = 'libraries/fancybox/jquery.fancybox.min';
      $this->data['assets_css'][] = 'libraries/hamburgers/hamburgers.min';
      $this->data['assets_css'][] = 'libraries/datatables/media/css/dataTables.bootstrap4.min';
      $this->data['assets_css'][] = 'css/unify-admin';
      $this->data['assets_css'][] = 'css/custom';

      $this->data['assets_js'][] = 'libraries/jquery/jquery.min';
      $this->data['assets_js'][] = 'libraries/jquery-migrate/jquery-migrate.min';
      $this->data['assets_js'][] = 'libraries/jquery/jquery-ui';
      $this->data['assets_js'][] = 'libraries/popper.min';
      $this->data['assets_js'][] = 'libraries/bootstrap/bootstrap.min';
      $this->data['assets_js'][] = 'libraries/cookiejs/jquery.cookie';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/widget';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/version';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/keycode';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/position';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/unique-id';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/safe-active-element';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/widgets/menu';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/widgets/mouse';
      $this->data['assets_js'][] = 'libraries/jquery-ui/ui/widgets/datepicker';
      $this->data['assets_js'][] = 'libraries/appear';
      $this->data['assets_js'][] = 'libraries/bootstrap-select/js/bootstrap-select.min';
      $this->data['assets_js'][] = 'libraries/bootstrap-tagsinput/js/bootstrap-tagsinput.min';
      $this->data['assets_js'][] = 'libraries/flatpickr/dist/js/flatpickr.min';
      $this->data['assets_js'][] = 'libraries/malihu-scrollbar/jquery.mCustomScrollbar.concat.min';
      $this->data['assets_js'][] = 'libraries/chartist-js/chartist.min';
      $this->data['assets_js'][] = 'libraries/chartist-js-tooltip/chartist-plugin-tooltip';
      $this->data['assets_js'][] = 'libraries/fancybox/jquery.fancybox.min';
      $this->data['assets_js'][] = 'libraries/datatables/media/js/jquery.dataTables.min';
      $this->data['assets_js'][] = 'libraries/datatables/media/js/dataTables.bootstrap4.min';
      $this->data['assets_js'][] = 'libraries/meiomask/meiomask.min';
			$this->data['assets_js'][] = 'libraries/tinymce/js/tinymce.min';
      $this->data['assets_js'][] = 'js/hs.core';
      $this->data['assets_js'][] = 'js/components/hs.side-nav';
      $this->data['assets_js'][] = 'js/helpers/hs.hamburgers';
      $this->data['assets_js'][] = 'js/components/hs.range-datepicker';
      $this->data['assets_js'][] = 'js/components/hs.datepicker';
      $this->data['assets_js'][] = 'js/components/hs.dropdown';
      $this->data['assets_js'][] = 'js/components/hs.scrollbar';
      $this->data['assets_js'][] = 'js/components/hs.area-chart';
      $this->data['assets_js'][] = 'js/components/hs.donut-chart';
      $this->data['assets_js'][] = 'js/components/hs.bar-chart';
      $this->data['assets_js'][] = 'js/helpers/hs.focus-state';
      $this->data['assets_js'][] = 'js/helpers/hs.file-attachments';
      $this->data['assets_js'][] = 'js/components/hs.file-attachement';
      $this->data['assets_js'][] = 'js/components/hs.popup';
      $this->data['assets_js'][] = 'js/mask';
      $pasta = (empty(strpos($_SERVER['REQUEST_URI'], 'fito'))) ? 'admin' : 'fito';
      $this->data['assets_js'][] = 'js/'.$pasta;
      $this->data['assets_js'][] = 'js/plugins.init';
      $this->data['assets_js'][] = 'js/tinymce';
      break;
    }
  }

  public function atualiza_css () {

    $secoes = $this->secoes_model->get_all();
    $css = '';

    foreach ($secoes as $secao) {
      list($r, $g, $b) = sscanf($secao['cor_fundo'], "#%02x%02x%02x");
      list($r2, $g2, $b2) = sscanf($secao['cor_fundo2'], "#%02x%02x%02x");
      $imagem = $this->data['uploads_url'] . 'site/secao/';
      $imagem = ($secao['foto_fundo']) ? $imagem . $secao['id'] . '/' . $secao['foto_fundo'] : $imagem.'transparent.png';
			
      $css .= '#'.$secao['classe'].' .divimage {
          background-image: linear-gradient(to right, rgba(' . $r . ', ' . $g . ', ' . $b . ', ' . ($secao['transparencia_fundo']/10) . '),
          rgba(' . $r2 . ', ' . $g2 . ', ' . $b2 . ', ' . ($secao['transparencia_fundo']/10) . ')), url(' . base_url() . $imagem  . ');
					height: 130%;
        }

    		#'.$secao['classe'].' .tag,
    		#'.$secao['classe'].' .titulo {
          color: '.$secao['titulo_cor'].';
        }
    		#'.$secao['classe'].' .subtitulo {
          color: '.$secao['subtitulo_cor'].';
        }
    		#'.$secao['classe'].' .componente_titulo {
          color: '.$secao['componente_titulo'].';
        }
    		#'.$secao['classe'].' .componente_subtitulo {
          color: '.$secao['componente_subtitulo'].';
        }
    		#'.$secao['classe'].' .componente_fundo1 {
          background-color: '.$secao['componente_fundo1'].';
        }
    		#'.$secao['classe'].' .componente_fundo2 {
          background-color: '.$secao['componente_fundo2'].';
        }'.PHP_EOL;
    }

    $cor_principal           = $this->settings_model->get(15);
    $cor_rodape              = $this->settings_model->get(16);
    $cor_rodape_icones_fonte = $this->settings_model->get(17);
    $cor_rodape_icones_fundo = $this->settings_model->get(18);
    $cor_admin_fonte         = $this->settings_model->get(19);
    $cor_admin_fundo         = $this->settings_model->get(20);

    $css .= '
      .u-accordion-color-primary .u-accordion__header [aria-expanded="true"],
      .nav-item.active .nav-link,
      .nav-item .nav-link:hover,
      .g-color-primary {
        color: '.$cor_principal['valor'].'!important;
      }
      .g-brd-primary--focus:focus,
      .g-brd-primary {
        border-color: '.$cor_principal['valor'].'!important;
      }
      .btn-outline-primary {
       color: '.$cor_principal['valor'].';
       border-color: '.$cor_principal['valor'].';
      }';

    list($r, $g, $b) = sscanf($cor_principal['valor'], "#%02x%02x%02x");

    $css .='
      .btn-outline-primary:not([disabled]):not(.disabled).active,
      .btn-outline-primary:not([disabled]):not(.disabled):active,
      .show>.btn-outline-primary.dropdown-toggle {
       box-shadow: 0 0 0 0.2rem rgba('.$r.', '.$g.', '.$b.',.5);
      }
      .btn-outline-primary:not([disabled]):not(.disabled).active,
      .btn-outline-primary:not([disabled]):not(.disabled):active,
      .show>.btn-outline-primary.dropdown-toggle,
      .btn-outline-primary:hover {
       background-color: '.$cor_principal['valor'].';
       border-color: '.$cor_principal['valor'].';
      }
      ::selection {
       color: #fff;
       background-color: '.$cor_principal['valor'].';
      }
      .g-bg-primary--hover:hover,
      [class*="u-nav-v8"] .nav-link.active,
      [class*="u-nav-v8"] .nav-link.active .u-nav-v8__icon,
      .u-carousel-indicators-v1 li.slick-active span,
      .u-btn-primary,
      .u-go-to-v1:hover,
      .u-go-to-v1:focus:hover {
        background-color: '.$cor_principal['valor'].'!important;
      }

      .cor_rodape {
        background-color: '.$cor_rodape['valor'].';
      }
      .cor_rodape_icones_fonte {
        color: '.$cor_rodape_icones_fonte['valor'].';
      }
      .cor_rodape_icones_fundo {
        background-color: '.$cor_rodape_icones_fundo['valor'].';
      }

      [class*="u-nav-v8"] .nav-link.active::before {
       background-image: -webkit-gradient(linear, right top, left bottom, color-stop(49.6%, transparent), color-stop(50%, '.$cor_principal['valor'].')), -webkit-gradient(linear, right bottom, left top, color-stop(49.6%, transparent), color-stop(50%, '.$cor_principal['valor'].'));
       background-image: -webkit-linear-gradient(top right, transparent 49.6%, '.$cor_principal['valor'].' 50%), -webkit-linear-gradient(bottom right, transparent 49.6%, '.$cor_principal['valor'].' 50%);
       background-image: -o-linear-gradient(top right, transparent 49.6%, '.$cor_principal['valor'].' 50%), -o-linear-gradient(bottom right, transparent 49.6%, '.$cor_principal['valor'].' 50%);
       background-image: linear-gradient(to bottom left, transparent 49.6%, '.$cor_principal['valor'].' 50%), linear-gradient(to top left, transparent 49.6%, '.$cor_principal['valor'].' 50%);
      }

      @media (min-width: 768px)
      .u-nav-v8-2 .nav-link.active::before {
        background-image: -webkit-gradient(linear, right top, left bottom, color-stop(49.6%, transparent), color-stop(50%, '.$cor_principal['valor'].')), -webkit-gradient(linear, right bottom, left top, color-stop(49.6%, transparent), color-stop(50%, '.$cor_principal['valor'].'));
        background-image: -webkit-linear-gradient(top right, transparent 49.6%, '.$cor_principal['valor'].' 50%), -webkit-linear-gradient(bottom right, transparent 49.6%, '.$cor_principal['valor'].' 50%);
        background-image: -o-linear-gradient(top right, transparent 49.6%, '.$cor_principal['valor'].' 50%), -o-linear-gradient(bottom right, transparent 49.6%, '.$cor_principal['valor'].' 50%);
        background-image: linear-gradient(to bottom left, transparent 49.6%, '.$cor_principal['valor'].' 50%), linear-gradient(to top left, transparent 49.6%, '.$cor_principal['valor'].' 50%);
      }
      ';
		
		$marcas = $this->marcas_model->where('ativo', 1)->get_all();
		foreach ($marcas as $marca) {
			$css .= '
				.marcas .marca a .logo.' . $marca['link'] . ' {
					background-image: url(' . base_url() .'uploads/site/marcas/' . $marca['logo'] . ');
				}
			';
		}

    write_file('assets/site/styles/secoes.css', $css);

    $name = $this->config->item('tag_title');
    $short_name = explode('-', $name);
    $short_name = trim( $short_name[0] );
    $manifest = '{
     "short_name"		: "' . $short_name . '",
     "name"				: "' . $name . '",
     "background_color"	: "' . $cor_principal['valor'] . '",
     "theme_color"		: "' . $cor_principal['valor'] . '",
     "icons"				: [
     {
       "src"		: "uploads/config/' . $this->config->item('favicon') . '",
       "sizes"		: "256x256",
       "type"		: "image/png"
     }
     ],
     "start_url"			: "/site",
     "orientation"		: "portrait",
     "display"			: "standalone"
    }';
    write_file('manifest.json', $manifest);
  }

}