$(document).on('ready', function () {
	
  $.HSCore.helpers.HSBgVideo.init('.js-bg-video');
	$.HSCore.components.HSCountQty.init('.js-quantity');
	
  // initialization of carousel
  $.HSCore.components.HSCarousel.init('.js-carousel');
  //$.HSCore.helpers.HSBgVideo.init('.js-bg-video');
	
  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  
	// initialization of tabs
  $.HSCore.components.HSTabs.init('[role="tablist"]');
  
	// initialization of go to section
  $.HSCore.components.HSGoTo.init('.js-go-to');
});

$(window).on('load', function() {
	
	$("#splash-screen").fadeOut(function() {
		// initialization of scroll animation
		$.HSCore.components.HSOnScrollAnimation.init('[data-animation]');
		
		var url = '#' + (window.location.href).split('#')[1];
		if (url != '#') {
			$('html,body').animate({scrollTop:$(url).offset().top}, 1000);
		}
	});
	
  // initialization of HSScrollNav
  $.HSCore.components.HSScrollNav.init($('.js-scroll-nav'), {
    duration: 700
  });
	// initialization of header
	$.HSCore.components.HSHeaderFullscreen.init($('#js-header'));
	$.HSCore.helpers.HSHamburgers.init('.hamburger');
	
	$(".scroll").click(function(event){
		$('html,body').animate({scrollTop:$(this.hash).offset().top}, 1000);
  });
	
});

$(window).on('scroll', function() {
	if ($(document).scrollTop() >= ( $('section:eq(1)').offset().top - 50 )) {
		$('#header-toggler .hamburger-inner').addClass('f-bg-marrom');
		$('#header-toggler div').addClass('f-color-marrom');
	} else {
		$('#header-toggler .hamburger-inner').removeClass('f-bg-marrom');
		$('#header-toggler div').removeClass('f-color-marrom');
	}
});

$(window).on('resize', function() {
  setTimeout(function () {
    $.HSCore.components.HSTabs.init('[role="tablist"]');
  }, 200);
});

$("#form-newsletter").on("submit", "#f-newsletter", function() {
  if ( $("#email").val() != "" ) {
    var dados = $("#f-newsletter").serialize();
    $.ajax({
      type: "POST",
      url: "enviar-newsletter",
      data: dados,
      beforeSend: function() {
        $("#f-newsletter button").html('<i class="fa fa-spinner fa-spin"></i>');
        $("#f-newsletter button").attr('disabled');
      },
      success: function(txt) {
        $("#f-newsletter button").html('cadastrar');
        $("#f-newsletter button").removeAttr('disabled');
        if (txt) {
          $("#f-newsletter").each(function(){
             this.reset();
          });
          alert("Obrigado por se cadastrar.");
        } else {
          alert('Ocorreu um erro, tente se cadastrar novamente!');
        }
      }
    });
    return false;
  } else {
    alert("Você deve digitar seu e-mail!");
    return false;
  }
});

$("#agenda").on("submit", "#f-quer-tocar", function() {
  if ( $("#email").val() != "" ) {
    var dados = $("#f-quer-tocar").serialize();
    $.ajax({
      type: "POST",
      url: "enviar-agenda",
      data: dados,
      beforeSend: function() {
        $("#f-quer-tocar button").html('<i class="fa fa-spinner fa-spin"></i>');
        $("#f-quer-tocar button").attr('disabled');
      },
      success: function(txt) {
        $("#f-quer-tocar button").html('enviar');
        $("#f-quer-tocar button").removeAttr('disabled');
        if (txt) {
          $("#f-quer-tocar").each(function(){
             this.reset();
          });
          alert("Obrigado pelo contato.");
        } else {
          alert('Ocorreu um erro, tente enviar novamente!');
        }
      }
    });
    return false;
  } else {
    alert("Você deve preencher todos os campos!");
    return false;
  }
});

$("#empreenda").on("submit", "#f-empreenda", function() {
  if ( $("#email").val() != "" ) {
    var dados = $("#f-empreenda").serialize();
    $.ajax({
      type: "POST",
      url: "enviar-empreenda",
      data: dados,
      beforeSend: function() {
        $("#f-empreenda button").html('<i class="fa fa-spinner fa-spin"></i>');
        $("#f-empreenda button").attr('disabled');
      },
      success: function(txt) {
        $("#f-empreenda button").html('enviar');
        $("#f-empreenda button").removeAttr('disabled');
        if (txt) {
          $("#f-empreenda").each(function(){
             this.reset();
          });
          alert("Obrigado pelo contato.");
        } else {
          alert('Ocorreu um erro, tente enviar novamente!');
        }
      }
    });
    return false;
  } else {
    alert("Você deve preencher todos os campos!");
    return false;
  }
});

$("#contato").on("submit", "#f-contato", function() {
  if (($("#nome").val() != "") && ($("#email").val() != "") && ($("#telefone").val() != "") && ($("#assunto").val() != "") && ($("#mensagem").val() != "")) {
    var dados = $("#f-contato").serialize();
    $.ajax({
      type: "POST",
      url: "enviar",
      data: dados,
      beforeSend: function() {
        $("#f-contato button").html('<i class="fa fa-spinner fa-spin fa-lg"></i>');
        $("#f-contato button").attr('disabled');
      },
      success: function(txt) {
        $("#f-contato button").html('enviar');
        $("#f-contato button").removeAttr('disabled');
        if (txt) {
          $("#f-contato").each(function(){
             this.reset();
          });
          alert("Obrigado por entrar em contato conosco.");
        } else {
          alert('Ocorreu um erro, tente enviar novamente!');
        }
      }
    });
    return false;
  } else {
    alert("Você deve preencher todos os campos!");
    return false;
  }
});