/**
 * Count quantity wrapper.
 *
 * @author Htmlstream
 * @version 1.0
 *
 */
;(function ($) {
  'use strict';

  $.HSCore.components.HSCountQty = {
    /**
     *
     *
     * @var Object _baseConfig
     */
    _baseConfig: {},

    /**
     *
     *
     * @var jQuery pageCollection
     */
    pageCollection: $(),

    /**
     * Initialization of Count quantity wrapper.
     *
     * @param String selector (optional)
     * @param Object config (optional)
     *
     * @return jQuery pageCollection - collection of initialized items.
     */

    init: function (selector, config) {

      this.collection = selector && $(selector).length ? $(selector) : $();
      if (!$(selector).length) return;

      this.config = config && $.isPlainObject(config) ?
          $.extend({}, this._baseConfig, config) : this._baseConfig;

      this.config.itemSelector = selector;

      this.initCountQty();

      return this.pageCollection;

    },

    initCountQty: function () {
      //Variables
      var $self = this,
          collection = $self.pageCollection;

      //Actions
      this.collection.each(function (i, el) {
        //Variables
        var $this = $(el),
            $plus = $this.find('.js-plus'),
            $minus = $this.find('.js-minus'),
            $result = $this.find('.js-result'),
            resultVal = parseInt($result.val()),
						qtd = ['',
									 /* 01 */ ['73CC5A853A3A477774556FB1BFC6FD2F', '117,90', '19,65'],
									 /* 02 */ ['0CAB0A1490901E1114CE6F87F8B8E24F', '235,80', '39,30'],
									 /* 03 */ ['72FF371B21218ACAA4F75F8E03F6D09A', '353,70', '58,95'],
									 /* 04 */ ['FC4FCFC84C4C161114FA4FAE5D04A5A4', '471,60', '78,60'],
									 /* 05 */ ['6F7AE1DC7F7FA34004473F9C9377AB28', '589,50', '98,25'],
									 /* 06 */ ['D6C20C2D4848109664045FB64652484C', '707,40', '117,90'],
									 /* 07 */ ['3AE32CF4A7A7C78774ED2FB912CDA484', '825,30', '137,55'],
									 /* 08 */ ['C132C68F4B4BE7D11414FF8E2E5992B3', '943,20', '157,20'],
									 /* 09 */ ['281F4731A7A75BFBB4A9CFBED0221AAC', '1.061,10', '176,85'],
									 /* 10 */ ['92DD2710A3A335AEE4F13FBC8D1A9AD3', '1.179,00', '196,50']];

        $plus.on('click', function (e) {
          e.preventDefault();

          if (resultVal < 10) {
						resultVal += 1;
          	$result.val(resultVal);
						$('#itemCode').val( qtd[resultVal][0] );
						$('#total').html( (qtd[resultVal][1]) );
						$('#parcela').html( qtd[resultVal][2] );
          } else {
						$('div.alert').removeClass('d-none');
            return false;
          }
        });

        $minus.on('click', function (e) {
          e.preventDefault();

          if (resultVal > 1) {
            resultVal -= 1;
            $result.val(resultVal);
						$('#itemCode').val(qtd[resultVal][0]);
						$('#total').html(qtd[resultVal][1]);
						$('#parcela').html(qtd[resultVal][2]);
						$('div.alert').fadeOut("fast", function() {
							$(this).addClass('d-none');
						});
          } else {
            return false;
          }
        });

        //Actions
        collection = collection.add($this);
      });
    }

  };

})(jQuery);