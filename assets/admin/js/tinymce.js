	tinymce.init({
	selector: "textarea",
	height: 250,
	menubar: false,
	valid_elements: '*[*]',
	plugins: [
		"advlist autolink link image lists charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking spellchecker",
		"table contextmenu directionality emoticons paste textcolor responsivefilemanager  codesample"
	],
	relative_urls: false,

	image_advtab: true,
	toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | removeformat ',
  content_css: [
    '//fonts.googleapis.com/css?family=Montserrat:400,400i,600,600i,700,700i,900,900i',
    '//www.spaziosapore.com.br/assets/site/styles/style.css',
    '//www.spaziosapore.com.br/assets/site/styles/custom.css'
  ]
});