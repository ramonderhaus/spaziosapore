$(document).on('ready', function () {

	var base_url = 'https://www.spaziosapore.com.br/',
		ok = 'TRUE';

	$(".sortable").sortable({
		opacity: 0.5,
		tolerance: 'pointer'
	});

	$("i.ativo").click(function () {
		var id = $(this).data('id'),
			table = $(this).data('table');
		if ($("#ativo" + id).is(':checked')) {
			$.ajax({
				type: "POST",
				url: base_url + "admin/dashboard/ativo",
				data: "ativo=0&id=" + id + "&table=" + table,
				success: function (txt) {
					console.log(txt);
				}
			});
		} else {
			$.ajax({
				type: "POST",
				url: base_url + "admin/dashboard/ativo",
				data: "ativo=1&id=" + id + "&table=" + table,
				success: function (txt) {
					console.log(txt);
				}
			});
		}
	});

	$('.js-datatable').dataTable({
		responsive: true,
		language: {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "",
			"sSearchPlaceholder": "Procurar",
			"oPaginate": {
				"sNext": ">",
				"sPrevious": "<",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		},
		createdRow: function(row, data, dataIndex){
       // Initialize custom control
       $('[data-toggle="tooltip"]', row).tooltip();
    }
	});

	$("#cadastro").submit(function () {
		if (valida_cpf_cnpj($("#cpf").val())) {
			ok = 'TRUE';
			console.log('TRUE: ' + ok);
			$('#cpf').blur();
			return true;
		} else {
			ok = 'FALSE';
			console.log('FALSE: ' + ok);
			alert("O CPF/CNPJ é inválido.");
			$("#cpf").focus();
			return false;
		}
		console.log(ok);
		return false;
	});

	$('#cpf').blur(function () {
		$("#status").html('');
		var cpf_cnpj = $(this).val();
		if (valida_cpf_cnpj(cpf_cnpj)) {
			$(this).val(formata_cpf_cnpj(cpf_cnpj));
			var numero = $(this).val();
			console.log(numero);
			$.ajax({
				type: "POST",
				url: base_url + "admin/clientes/valida_cpf_cnpj",
				data: "cpf_cnpj=" + numero,
				beforeSend: function () {
					$("#status").html('<i class="fa fa-spinner fa-spin fa-lg"></i>');
				},
				success: function (txt) {
					console.log(txt);
					if (txt > 0) {
						$("#status").html('Este CPF / CNPJ já está cadastrado. <a href="./form/' + txt + '">Clique aqui</a>');
					} else {
						$("#status").html('');
					}
				}
			});
		} else {
			$("#status").html('Este CPF / CNPJ é inválido.');
		}
	});

	$('#cpf').keyup(function () {
		$("#status").html('');
		$(this).val(this.value.replace(/[^0-9]/g, ''));
	});
	$('#cpf, .cep, .cel, .tel').focus(function () {
		$(this).val(this.value.replace(/[^0-9]/g, ''));
	});
	$('.cep').blur(function () {
		$(this).val(formata_cep($(this).val()));
	});
	$('.cep').keyup(function () {
		var input = $("form").find('input:visible');
		var indice = input.index(event.target) + 1;
		if ($(this).val().length === 8) {
			$(input[indice]).focus();
		}
	});

	$('.cel').blur(function () {
		$(this).val(formata_cel($(this).val()));
	});
	$('.cel').keyup(function () {
		var input = $("form").find('input:visible');
		var indice = input.index(event.target) + 1;
		if ($(this).val().length === 11) {
			$(input[indice]).focus();
		}
	});

	$('.tel').blur(function () {
		$(this).val(formata_tel($(this).val()));
	});
	$('.tel').keyup(function () {
		var input = $("form").find('input:visible');
		var indice = input.index(event.target) + 1;
		$(this).val(this.value.replace(/[^0-9]/g, ''));
		if ($(this).val().length === 10) {
			$(input[indice]).focus();
		}
	});

	$(".remove").click(function (e) {
		e.preventDefault();
		if (confirm('Tem certeza que deseja excluir este registro?')) {
			$(window.document.location).attr('href', this.href);
		}
	});

	$("form").submit(function () {
		if (ok === 'TRUE') {
			$(this).find("[type=submit]").html('<i class="fa fa-spinner fa-pulse fa-spin fa-lg"></i>');
			$(this).find("[type=submit]").attr('disabled', 'disabled');
		}
	});

	// show the alert
	$(".alert-dismissable").fadeTo(5000, 500).slideUp(500, function () {
		$(".alert-dismissable").alert('close');
	});
});
